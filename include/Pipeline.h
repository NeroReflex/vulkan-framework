#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Pipeline :
			virtual public DeviceOwned {

		public:
			enum class PipelineType {
				Compute,
				Graphics,
			};

			Pipeline() = delete;

			Pipeline(const Pipeline&) = delete;

			Pipeline(Pipeline&&) = delete;

			Pipeline& operator=(const Pipeline&) = delete;

			~Pipeline() override;

			const VkPipeline& getNativePipelineHandle() const noexcept;

			const VkPipelineLayout& getNativePipelineLayoutHandle() const noexcept;

			const VkDescriptorSetLayout& getNativeDescriptorSetLayout() const noexcept;

		protected:
			Pipeline(Device* device, PipelineType type, VkPipelineLayout&& pipelineLayout, VkDescriptorSetLayout&& descriptorSetLayout, VkPipeline&& pipeline) noexcept;

		private:
			PipelineType mType;

			VkPipeline mPipeline;

			VkPipelineLayout mPipelineLayout;

			VkDescriptorSetLayout mDescriptorSetLayout;
		};
	}
}