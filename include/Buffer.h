#pragma once

#include "DeviceOwned.h"
#include "SpaceRequiringResource.h"

namespace NeroReflex {
	namespace VulkanFramework {
		class Buffer :
			virtual public DeviceOwned,
			virtual public SpaceRequiringResource {

		public:
			Buffer(Device* const device, VkBufferUsageFlags usage, VkDeviceSize size, VkBuffer&& buffer) noexcept;

			Buffer(const Buffer&) = delete;

			Buffer(Buffer&&) = delete;

			Buffer& operator=(const Buffer&) = delete;

			~Buffer();

			const VkBuffer& getNativeBufferHandle() const noexcept;

			const VkBufferUsageFlags& getBufferUsage() const noexcept;

			const VkDeviceSize& getBufferSize() const noexcept;

		protected:
			std::unique_ptr<VkMemoryRequirements> queryMemoryRequirements() const noexcept final;

			void bindMemory(MemoryPool* memoryPool, VkDeviceSize startingOffset) noexcept final;

		private:
			VkBufferUsageFlags mUsage;

			VkDeviceSize mSize;

			VkBuffer mBuffer;
		};
	}
}