#pragma once

#include "Pipeline.h"

#include "RenderPass.h"

#include "Utils/SurfaceDimensions.h"
#include "Utils/DepthStencilConfiguration.h"
#include "Utils/Rasterizer.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class GraphicPipeline :
			public Pipeline {

		public:
			GraphicPipeline(
				Device* device,
				VkPipelineLayout&& pipelineLayout,
				VkDescriptorSetLayout&& descriptorSetLayout,
				VkPipeline&& pipeline,
				Utils::Rasterizer&& rasterizer,
				Utils::DepthStencilConfiguration&& depthStencilConfig,
				Utils::SurfaceDimensions surfaceDimensions,
				const RenderPass* const renderPass
			) noexcept;

			GraphicPipeline(const GraphicPipeline&) = delete;

			GraphicPipeline(GraphicPipeline&&) = delete;

			GraphicPipeline& operator=(const GraphicPipeline&) = delete;

			~GraphicPipeline() override;

			const RenderPass* getRenderPass() const noexcept;

			const Utils::Rasterizer& getRasterizer() const noexcept;

			const Utils::DepthStencilConfiguration& getDepthStencilConfiguration() const noexcept;

		private:
			Utils::Rasterizer mRasterizer;

			Utils::SurfaceDimensions mSurfaceDimensions;

			const RenderPass* const mRenderPass;

			Utils::DepthStencilConfiguration mDepthStencilConfig;
		};
	}
}