#pragma once

#include "VulkanFramework.h"

namespace NeroReflex {
	namespace VulkanFramework {

		/**
		 * Used to represent a vertex shader layout (location = <index>) in <type> name;
		 */
		class VertexInputAttribute {

		public:
			
			enum AttributeType {
				floatType = VK_FORMAT_R32_SFLOAT,
				vec1Type = VK_FORMAT_R32_SFLOAT,
				vec2Type = VK_FORMAT_R32G32_SFLOAT,
				vec3Type = VK_FORMAT_R32G32B32_SFLOAT,
				vec4Type = VK_FORMAT_R32G32B32A32_SFLOAT,

				uintType = VK_FORMAT_R32_UINT,
				uvec1Type = VK_FORMAT_R32_UINT,
				uvec2Type = VK_FORMAT_R32G32_UINT,
				uvec3Type = VK_FORMAT_R32G32B32_UINT,
				uvec4Type = VK_FORMAT_R32G32B32A32_UINT,

				intType = VK_FORMAT_R32_SINT,
				ivec1Type = VK_FORMAT_R32_SINT,
				ivec2Type = VK_FORMAT_R32G32_SINT,
				ivec3Type = VK_FORMAT_R32G32B32_SINT,
				ivec4Type = VK_FORMAT_R32G32B32A32_SINT,
			};

			/**
			 * Create a vertex input attribute to be passed to VertexInputBinding.
			 *
			 * @param location the location in the vertex shader (the <index> in the class descriptio)
			 * @param offset the data offset withing the current binding (e.g. offsetof(Vertex, position) or offsetof(Vertex, normal) )
			 * @param type the type of the attribute (e.g. vec4 in the vertex shader)
			 */
			VertexInputAttribute(uint32_t location, uint32_t offset, AttributeType type) noexcept;

			VkVertexInputAttributeDescription generateNativeHandler(uint32_t binding) const noexcept;


			void setBinding(uint32_t binding) const noexcept;

		private:
			static VkFormat transformToVkFormat(AttributeType type) noexcept;

			const uint32_t mLocation;

			const uint32_t mOffset;

			const VkFormat mFormat;
		};

	}
}
