#pragma once

#include "VulkanFramework.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Device;

		class DeviceOwned {

		public:
			DeviceOwned(Device* device) noexcept;

			DeviceOwned(DeviceOwned&) = delete;

			DeviceOwned& operator=(DeviceOwned&) = delete;

			virtual ~DeviceOwned();

			Device* getParentDevice() const noexcept;

		private:
			Device* mOwningDevice;
		};

	}
}