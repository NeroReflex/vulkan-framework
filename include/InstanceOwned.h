#pragma once

#include "Instance.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class InstanceOwned {

		public:
			InstanceOwned(const Instance* instance) noexcept;

			InstanceOwned(InstanceOwned&) = delete;

			InstanceOwned& operator=(InstanceOwned&) = delete;

			virtual ~InstanceOwned();

			const Instance* getParentInstance() const noexcept;

		private:
			const Instance* mOwningInstance;
		};

	}
}