#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {

		/**
		 * This class acts as an interface for physical memory allocated.
		 */
		class DeviceMemoryBuffer :
			virtual public DeviceOwned {

		public:
			DeviceMemoryBuffer(Device* device) noexcept;

			DeviceMemoryBuffer(const DeviceMemoryBuffer&) = delete;

			DeviceMemoryBuffer(DeviceMemoryBuffer&&) = delete;

			DeviceMemoryBuffer& operator=(const DeviceMemoryBuffer&) = delete;

			~DeviceMemoryBuffer() override;
		};

	}
}
