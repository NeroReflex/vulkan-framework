#pragma once

#include "DeviceOwned.h"
#include "Shaders/ShaderType.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class ShaderLayoutBinding {

		public:
			enum class BindingType {
				Sampler,
				SampledImage,
				CombinedImageSampler,
				StorageImage,
				UniformTexelStorage,
				UniformTexelBuffer,
				UniformBuffer,
				StorageBuffer,
				InputAttachment,
			};

			struct BindingDescriptor {
				BindingType bindingType;
				uint32_t bindingPoint;
				uint32_t count;
				std::vector<Shaders::ShaderType> bindingPipelineStage;

				/**
				 * Create a binding descriptor.
				 *
				 * @param bindingType the type of the binding
				 * @param binding point the layout location
				 * @param count the number of bindings accessed as an array or the number of bytes for a uniform buffer
				 */
				inline BindingDescriptor(
					BindingType bindingType,
					uint32_t bindingPoint,
					uint32_t count,
					const std::vector<Shaders::ShaderType>& bindingPipelineStage = std::vector<Shaders::ShaderType>()) noexcept
					: bindingType(bindingType), bindingPoint(bindingPoint), count(count), bindingPipelineStage(bindingPipelineStage) {};
			};

			struct PushConstantDescriptor {
				uint32_t offset;

				uint32_t size;

				std::vector<Shaders::ShaderType> bindingPipelineStage;

				/**
				 * Create a push constant descriptor.
				 *
				 * @param bindingType the type of the binding
				 * @param binding point the layout location
				 * @param count the number of bindings accessed as an array or the number of bytes for a uniform buffer
				 */
				inline PushConstantDescriptor(
					uint32_t offset,
					uint32_t size,
					const std::vector<Shaders::ShaderType>& bindingPipelineStage = std::vector<Shaders::ShaderType>()) noexcept
					: offset(offset), size(size), bindingPipelineStage(bindingPipelineStage) {};
			};

			ShaderLayoutBinding(
				const std::vector<BindingDescriptor>& descriptors = std::vector<BindingDescriptor>(),
				const std::vector<PushConstantDescriptor>& pushConstants = std::vector<PushConstantDescriptor>()
			) noexcept;

			ShaderLayoutBinding(const ShaderLayoutBinding&) noexcept;

			ShaderLayoutBinding(ShaderLayoutBinding&&) noexcept;

			ShaderLayoutBinding& operator=(const ShaderLayoutBinding&) noexcept;

			~ShaderLayoutBinding();

			std::vector<VkDescriptorSetLayoutBinding> getNativeLayoutHandles() const noexcept;

			std::vector<VkPushConstantRange> getNativePushContantsHandles() const noexcept;

		private:
			void insert(const BindingDescriptor& binding) noexcept;

			void insertPushConstant(const PushConstantDescriptor& pushConstantRangeSize) noexcept;

			std::vector<VkDescriptorSetLayoutBinding> mDescriptors;
			
			std::vector<VkPushConstantRange> mPushConstants;
		};
	}
}