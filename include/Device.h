#pragma once

#include "InstanceOwned.h"
#include "Swapchain.h"
#include "ShaderLayoutBinding.h"
#include "Pipeline.h"
#include "Image.h"
#include "Buffer.h"
#include "CommandPool.h"
#include "MemoryPool.h"
#include "DescriptorPool.h"
#include "Fence.h"
#include "Semaphore.h"
#include "RenderPass.h"
#include "Sampler.h"

#include "VertexInputBinding.h"

#include "Shaders/ComputeShader.h"
#include "Shaders/VertexShader.h"
#include "Shaders/GeometryShader.h"
#include "Shaders/FragmentShader.h"

#include "ComputePipeline.h"
#include "GraphicPipeline.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Device :
			virtual public InstanceOwned {

			friend class DeviceOwned;

		public:
			struct SwapChainSupportDetails {
				VkSurfaceCapabilitiesKHR capabilities;
				std::vector<VkSurfaceFormatKHR> formats;
				std::vector<VkPresentModeKHR> presentModes;
			};

			class SwapchainSelector {
			public:
				virtual VkSwapchainCreateInfoKHR operator()(const SwapChainSupportDetails&) const noexcept;

			private:
				VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) const noexcept;
				VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) const noexcept;
			};

			Device(
				const Instance* instance,
				VkPhysicalDevice&& physicalDevice,
				VkDevice&& device,
				std::vector<std::tuple<QueueFamily::ConcreteQueueFamilyDescriptor, uint32_t>> requiredQueueFamilyCollection
			) noexcept;

			Device(Device&) = delete;

			Device& operator=(Device&) = delete;

			~Device() override;

			const QueueFamily* getQueueFamily(uint32_t index) const noexcept;

			/**
			 * Create the swapchain on the current device
			 *
			 * @param queueFamilyCollection the list of queue families that will access images on the swapchain
			 * 
			 */
			Swapchain* createSwapchain(VkSurfaceKHR surface, std::vector<const QueueFamily*> queueFamilyCollection, uint32_t width, uint32_t height, const SwapchainSelector& selector = Device::SwapchainSelector()) noexcept;

			CommandPool* createCommandPool(const QueueFamily* queueFamily) noexcept;

			bool isExtensionAvailable(const std::string& extName) const noexcept;

			const VkDevice& getNativeDeviceHandle() const noexcept;

			const VkPhysicalDevice& getNativePhysicalDeviceInstance() const noexcept;

			MemoryPool* createMemoryPool(VkMemoryPropertyFlagBits props, VkDeviceSize pagesCount, uint32_t memoryTypeBits) noexcept;

			MemoryPool* createMemoryPool(VkMemoryPropertyFlagBits props, const std::vector<const SpaceRequiringResource*>& unallocatedResources, VkDeviceSize freePagesCount) noexcept;

			Shaders::ComputeShader* loadComputeShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept;

			Shaders::VertexShader* loadVertexShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept;

			Shaders::GeometryShader* loadGeometryShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept;

			Shaders::FragmentShader* loadFragmentShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept;

			DescriptorPool* createDescriptorPool(const std::vector<std::tuple<ShaderLayoutBinding::BindingType, uint32_t>>& descriptorPoolSize, uint32_t maxSets) noexcept;

			ComputePipeline* createComputePipeline(const Shaders::ComputeShader* shader) noexcept;

			RenderPass* createRenderPass(
				const std::vector<VkAttachmentDescription>& attachments = std::vector<VkAttachmentDescription>(),
				const std::vector<RenderPass::RenderSubPass>& subpasses = std::vector<RenderPass::RenderSubPass>(),
				const std::vector<VkSubpassDependency>& subpassesDependencies = std::vector<VkSubpassDependency>()
			) noexcept;

			/**
			 * Creates a graphic pipeline meant for drawing triangles.
			 *
			 */
			GraphicPipeline* createGraphicPipeline(
				const RenderPass* renderPass,
				uint32_t subpassNumber,
				const std::vector<const Shaders::Shader*>& shaders,
				Utils::Rasterizer rasterizer,
				Utils::DepthStencilConfiguration depthStencilConfig,
				const Utils::SurfaceDimensions& surfaceDimensions,
				const std::vector<VertexInputBinding>& bindings) noexcept;

			Image* createImage(std::vector<const QueueFamily*> queueFamilyCollection, VkImageUsageFlags usage, Image::ImageType type, uint32_t width, uint32_t height = 1, uint32_t depth = 1, VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT, uint32_t mipLevels = 1, VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT) noexcept;

			Fence* createFence(bool signaled = false) noexcept;

			Semaphore* createSemaphore() noexcept;

			Buffer* createBuffer(std::vector<const QueueFamily*> queueFamilyCollection, VkBufferUsageFlags usage, VkDeviceSize size) noexcept;

			Sampler* createSampler(Sampler::Filtering magFilter, Sampler::Filtering minFilter, Sampler::MipmapMode mipmapMode, float maxAnisotropy = 0.0f/*, float mipLodBias, float minLod, float maxLod*/) noexcept;

			Framebuffer* createFramebuffer(
				const RenderPass* const renderPass,
				const Utils::SurfaceDimensions& surfaceDimensions,
				const std::vector<ImageView*>& imageViews) noexcept;

			void waitForFences(std::vector<const Fence*> fences, uint64_t timeout = std::numeric_limits<uint64_t>::max()) const noexcept;

			void waitIdle() const noexcept;

			void destroy(DeviceOwned* obj) noexcept;

		private:
			uint32_t findHeap(uint32_t memoryTypeBits, VkMemoryPropertyFlags properties) const noexcept;

			VkPhysicalDevice mPhysicalDevice;

			VkDevice mDevice;

			std::vector<VkExtensionProperties> mAvailableExtensions;

			SwapChainSupportDetails mSupportedSwapchain;

			std::vector<QueueFamily*> mQueueFamilies;

			std::unordered_map<uintptr_t, std::unique_ptr<DeviceOwned>> mOwnedObjects;
		};

	}
}
