#pragma once

#include "DeviceMemoryBuffer.h"
#include "SwapchainImage.h"
#include "Utils/SurfaceDimensions.h"
#include "Queue.h"
#include "Fence.h"
#include "Semaphore.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Swapchain :
			virtual public DeviceMemoryBuffer {

		public:
			Swapchain(Device* device, VkSwapchainKHR&& swapchain, VkFormat format, Utils::SurfaceDimensions surfaceDimensions) noexcept;

			Swapchain(Swapchain&) = delete;

			Swapchain& operator=(Swapchain&) = delete;

			~Swapchain() override;

			const Utils::SurfaceDimensions& getSurfaceDimensions() const noexcept;

			const VkFormat& getFormat() const noexcept;

			size_t getImagesCount() const noexcept;

			const VkSwapchainKHR& getNativeSwapchainHandle() const noexcept;

			SwapchainImage* getSwapchainImageByIndex(uint32_t index) const noexcept;

			uint32_t acquireNextImage(const Semaphore* semaphore = nullptr, const Fence* fence = nullptr) const noexcept;

			void presentImageByIndex(const Queue* queue, uint32_t index, std::vector<const Semaphore*> waitSemaphores = {}) const noexcept;

		private:
			VkSwapchainKHR mSwapchain;

			VkFormat mFormat;

			std::vector<std::unique_ptr<SwapchainImage>> mImages;

			Utils::SurfaceDimensions mSurfaceDimensions;

		};
	}
}
