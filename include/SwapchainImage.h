#pragma once

#include "ImageInterface.h"

namespace NeroReflex {
	namespace VulkanFramework {
		class Swapchain;

		/**
		 * Represents an image on the swapchain.
		 */
		class SwapchainImage final :
			virtual public ImageInterface {

		public:
			SwapchainImage(Device* device, const Swapchain* swapchain, VkImage&& image) noexcept;

			SwapchainImage(const SwapchainImage&) = delete;

			SwapchainImage(SwapchainImage&&) = delete;

			SwapchainImage& operator=(const SwapchainImage&) = delete;

			~SwapchainImage() final;

			const Swapchain* getParentSwapchain() const noexcept;

			ImageView* getImageView() const noexcept;

			const VkFormat& getFormat() const noexcept final;

		private:
			const Swapchain* mParentSwapchain;

			std::unique_ptr<ImageView> mImageView;
		};
	}
}
