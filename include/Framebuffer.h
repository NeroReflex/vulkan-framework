#pragma once

#include "DeviceOwned.h"
#include "ImageView.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Framebuffer :
			virtual public DeviceOwned {

		public:
			Framebuffer(Device* const, VkFramebuffer&& framebuffer, const std::vector<ImageView*>& mAttachedImageViews) noexcept;

			Framebuffer(const Framebuffer&) = delete;

			Framebuffer(Framebuffer&&) = delete;

			Framebuffer& operator=(const Framebuffer&) = delete;

			virtual ~Framebuffer();

			const std::vector<ImageView*>& getAttachmentImageViews() const noexcept;

			const VkFramebuffer& getNativeFramebufferHandle() const noexcept;

		private:
			VkFramebuffer mFramebuffer;

			std::vector<ImageView*> mAttachedImageViews;
		};
	}
}
