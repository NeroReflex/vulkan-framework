#pragma once

#include "DeviceOwned.h"
#include "CommandBuffer.h"

namespace NeroReflex {
	namespace VulkanFramework {
		
		/**
		 * A pool of commands that command buffers can be created from.
		 */
		class CommandPool :
			virtual public DeviceOwned {

		public:
			CommandPool(Device* const, VkCommandPool&& commandPool) noexcept;

			CommandPool(const CommandPool&) = delete;

			CommandPool(CommandPool&&) = delete;

			CommandPool& operator=(const CommandPool&) = delete;

			~CommandPool() override;

			const VkCommandPool& getNativeCommandPoolHandle() const noexcept;

			CommandBuffer* createCommandBuffer() noexcept;

			std::vector<CommandBuffer*> createCommandBuffers(uint32_t count) noexcept;

		private:
			VkCommandPool mCommandPool;

			std::vector<std::unique_ptr<CommandBuffer>> mRegisteredCommandBuffers;
		};
	}
}
