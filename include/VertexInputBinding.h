#pragma once

#include "VertexInputAttribute.h"

namespace NeroReflex {
	namespace VulkanFramework {
		
		/**
		 * A vertex binding describes at which rate to load data from memory throughout the vertices.
		 * It specifies the number of bytes between data entries and whether to move to the next data entry after each vertex or after each instance.
		 */
		class VertexInputBinding {

		public:
			/**
			 * Creates a vertex input binding.
			 *
			 * @param stride number of bytes from this entry to the next (e.g. sizeof(Vertex))
			 * @param inputRate can either be VK_VERTEX_INPUT_RATE_VERTEX ot VK_VERTEX_INPUT_RATE_INSTANCE
			 * @param attributes vertex shader input attributes list
			 */
			VertexInputBinding(uint32_t stride, VkVertexInputRate inputRate, const std::vector<VertexInputAttribute>& attributes) noexcept;

			/**
			 * Generate native vulkan structures to fill a VkPipelineVertexInputStateCreateInfo structure.
			 *
			 * @param binding the binding number described in both: https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkVertexInputBindingDescription.html and https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkVertexInputAttributeDescription.html
			 */
			std::tuple<VkVertexInputBindingDescription, std::vector<VkVertexInputAttributeDescription>> generateNativeHandlers(uint32_t binding) const noexcept;

		private:
			const uint32_t mStride;

			const VkVertexInputRate mInputRate;

			const std::vector<VertexInputAttribute> mAttributes;
		};

	}
}
