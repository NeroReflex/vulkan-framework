#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Sampler :
			virtual public DeviceOwned {

		public:

			enum class Filtering {
				Nearest,
				Linear,
				Cubic
			};

			enum class MipmapMode {
				ModeNearest,
				ModeLinear
			};

			Sampler(
				Device* const device,
				VkSampler&& sampler,
				Filtering magFilter,
				Filtering minFilter,
				Sampler::MipmapMode mipmapMode,
				float maxAnisotropy
			) noexcept;

			Sampler(const Sampler&) = delete;

			Sampler(Sampler&&) = delete;

			Sampler& operator=(const Sampler&) = delete;

			~Sampler() override;

			const VkSampler& getNativeSamplerHandle() const noexcept;

			const Filtering& getMagFilter() const noexcept;

			const Filtering& getMinFilter() const noexcept;

			const MipmapMode& getMipmapMode() const noexcept;

			float getMaxAnisotropy() const noexcept;

			bool isAnisotropicEnabled() const noexcept;

		private:
			VkSampler mSampler;

			const Filtering mMagFilter, mMinFilter;

			const Sampler::MipmapMode mMipmapMode;

			const float mMaxAnisotropy;
		};
	}
}