#pragma once

#include "VulkanFramework.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Utils {

			class SurfaceDimensions {

			public:

				inline SurfaceDimensions(uint32_t width, uint32_t height) noexcept : mWidth(width), mHeight(height) {};

				inline const uint32_t& getWidth() const noexcept {
					return mWidth;
				};

				inline const uint32_t& getHeight() const noexcept {
					return mHeight;
				};

				inline bool operator==(const SurfaceDimensions& cmp) const noexcept {
					return (mWidth == cmp.mWidth) && (mHeight == cmp.mHeight);
				}

				inline bool operator!=(const SurfaceDimensions& cmp) const noexcept {
					return !(this->operator==(cmp));
				}

			private:
				uint32_t mWidth;

				uint32_t mHeight;
			};

		}
	}
}