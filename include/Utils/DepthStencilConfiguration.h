#pragma once

#include "VulkanFramework.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Utils {

			class DepthStencilConfiguration {

			public:

				static const float defaultLineWidth;

				enum PolygonModeType {
					PolygonFill,
					PolygonLine,
					PolygonPoint,
				};

				DepthStencilConfiguration(
					bool enableDepthTesting = false,
					bool depthWriteEnable = true,
					VkCompareOp depthCompareOperation = VkCompareOp::VK_COMPARE_OP_LESS,
					bool depthBoundsTestEnable = false,
					float minDepthBounds = 0.0,
					float maxDepthBounds = 1.0,
					bool stencilTestEnable = false,
					VkStencilOpState front = {},
					VkStencilOpState back = {}
				) noexcept;

				bool getEnableDepthTesting() const noexcept;

				bool getDepthWriteEnable() const noexcept;

				VkCompareOp getDepthCompareOperation() const noexcept;

				bool getDepthBoundsTestEnable() const noexcept;

				bool getStencilTestEnable() const noexcept;

				VkStencilOpState getFront() const noexcept;

				VkStencilOpState getBack() const noexcept;

				float getMinDepthBounds() const noexcept;

				float getMaxDepthBounds() const noexcept;

			private:
				bool mEnableDepthTesting;
				bool mDepthWriteEnable;
				VkCompareOp mDepthCompareOperation;
				bool mDepthBoundsTestEnable;
				bool mStencilTestEnable;
				VkStencilOpState mFront;
				VkStencilOpState mBack;
				float mMinDepthBounds;
				float mMaxDepthBounds;
			};

		}
	}
}