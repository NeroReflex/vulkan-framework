#pragma once

#include "VulkanFramework.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Utils {

			class Rasterizer {

			public:

				static const float defaultLineWidth;

				enum PolygonModeType {
					PolygonFill,
					PolygonLine,
					PolygonPoint,
				};

				Rasterizer(
					VkCullModeFlags cullMode = VK_CULL_MODE_NONE,
					VkFrontFace mFrontFace = VkFrontFace::VK_FRONT_FACE_COUNTER_CLOCKWISE,
					PolygonModeType polygonModeType = PolygonModeType::PolygonFill,
					float lineWidth = Rasterizer::defaultLineWidth,
					bool enableDepthClamp = false,
					bool enableDepthBias = false,
					float mDepthBiasCostantFactor = 0.0,
					float mDepthBiasClamp = 0.0,
					float mDepthBiasSlopeFactor = 0.0,
					const std::optional<VkConservativeRasterizationModeEXT>& conservativeRasterizationSettings = std::optional<VkConservativeRasterizationModeEXT>()
				) noexcept;

				const VkPipelineRasterizationStateCreateInfo& getNativeRasterizationCreateInfoHandler() const noexcept;

			private:
				VkPipelineRasterizationConservativeStateCreateInfoEXT mConservativeRasterizationState;

				VkPipelineRasterizationStateCreateInfo mCreateInfo;
			};

		}
	}
}