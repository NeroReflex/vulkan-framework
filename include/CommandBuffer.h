#pragma once

#include "Fence.h"
#include "Semaphore.h"
#include "Queue.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class CommandPool;

		class CommandBuffer {

		public:
			CommandBuffer(CommandPool* const parentCommandPool, VkCommandBuffer&& commandBuffer) noexcept;

			CommandBuffer(const CommandBuffer&) = delete;

			CommandBuffer(CommandBuffer&&) = delete;

			CommandBuffer& operator=(const CommandBuffer&) = delete;

			~CommandBuffer();

			void registerCommands(std::function<void(const VkCommandBuffer & commandBuffer)> fn) noexcept;

			void registerCommands(std::function<void(CommandBuffer* commandBuffer)> fn) noexcept;

			void submit(const Queue* const queue, const Fence* const fence = nullptr, std::vector<const Semaphore*> signalSemaphores = {}, std::vector<std::tuple<const Semaphore*, VkPipelineStageFlags>> waitSemaphores = {}) noexcept;

		private:
			VkCommandBuffer mCommandBuffer;

			const CommandPool* const mParentCommandPool;
		};
	}
}
