#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Semaphore :
			virtual public DeviceOwned {

		public:
			Semaphore(Device* const device, VkSemaphore&& semaphore) noexcept;

			Semaphore(const Semaphore&) = delete;

			Semaphore(Semaphore&&) = delete;

			Semaphore& operator=(const Semaphore&) = delete;

			~Semaphore() override;

			const VkSemaphore& getNativeSemaphoreHandle() const noexcept;

		private:
			VkSemaphore mSemaphore;
		};

	}
}