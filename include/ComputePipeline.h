#pragma once

#include "Pipeline.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class ComputePipeline :
			public Pipeline {

		public:
			ComputePipeline(Device* device, VkPipelineLayout&& pipelineLayout, VkDescriptorSetLayout&& descriptorSetLayout, VkPipeline&& pipeline) noexcept;

			ComputePipeline(const ComputePipeline&) = delete;

			ComputePipeline(ComputePipeline&&) = delete;

			ComputePipeline& operator=(const ComputePipeline&) = delete;

			~ComputePipeline() override = default;
		};
	}
}