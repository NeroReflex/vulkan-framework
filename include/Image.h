#pragma once

#include "ImageInterface.h"
#include "SpaceRequiringResource.h"
#include "ImageView.h"

namespace NeroReflex {
	namespace VulkanFramework {

		/**
		 * Represents a buffer used to store an image or texture.
		 */
		class Image final :
			virtual public SpaceRequiringResource,
			public ImageInterface {

		public:
			enum class ImageType {
				Image1D,
				Image2D,
				Image3D
			};

			Image(Device* device, ImageType type, VkFormat format, VkExtent3D extent, VkSampleCountFlagBits samples, uint32_t mipLevels, VkImage&& image) noexcept;

			Image(const Image&) = delete;

			Image(Image&&) = delete;

			Image& operator=(const Image&) = delete;

			~Image() final;

			/**
			 * Creates an image view of the current image.
			 *
			 * An image view is aportion of an image.
			 * An image view cannot differ from the base image unless the proper bit is set while creating the image.
			 */
			ImageView* createImageView(
				ImageView::ViewType type,
				std::optional<VkFormat> format = std::optional<VkFormat>(),
				VkImageAspectFlagBits subrangeAspectBits = VK_IMAGE_ASPECT_COLOR_BIT,
				ImageView::ViewColorMapping swizzle = ImageView::ViewColorMapping::rgba_rgba,
				std::optional<uint32_t> subrangeBaseMipLevel = std::optional<uint32_t>(),
				std::optional<uint32_t> subrangeLevelCount = std::optional<uint32_t>(),
				uint32_t subrangeBaseArrayLayer = 0,
				uint32_t subrangeLayerCount = 1
			) noexcept;

			/**
			 * @brief Destroy an image view created from the current instance
			 * 
			 * @param imgView the image view to destroy
			 */
			void destroyImageView(ImageView* imgView) noexcept;

			const VkFormat& getFormat() const noexcept override;

			const VkExtent3D& getExtent() const noexcept;

			uint32_t getMipLevels() const noexcept;

			VkSampleCountFlagBits getSamplesCount() const noexcept;

		protected:
			std::unique_ptr<VkMemoryRequirements> queryMemoryRequirements() const noexcept final;

			void bindMemory(MemoryPool* memoryPool, VkDeviceSize startingOffset) noexcept final;

		private:
			ImageType mType;

			VkExtent3D mExtent;

			VkFormat mFormat;

			uint32_t mMipLevels;

			VkSampleCountFlagBits mSamples;

			std::unordered_map<uintptr_t, std::unique_ptr<ImageView>> mImageViews;
		};
	}
}