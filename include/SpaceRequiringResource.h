#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Device;
		class MemoryPool;

		class SpaceRequiringResource:
			virtual public DeviceOwned {

				friend class MemoryPool;

		public:
			SpaceRequiringResource(Device* device) noexcept;

			SpaceRequiringResource(const SpaceRequiringResource&) = delete;

			SpaceRequiringResource(SpaceRequiringResource&&) = delete;

			SpaceRequiringResource& operator=(const SpaceRequiringResource&) = delete;

			virtual ~SpaceRequiringResource();

			const VkMemoryRequirements& getMemoryRequirements() const noexcept;

			MemoryPool* getAllocationMemoryPool() noexcept;

			const VkDeviceSize& getAllocationOffset() const noexcept;

		protected:
		
			virtual std::unique_ptr<VkMemoryRequirements> queryMemoryRequirements() const noexcept = 0;

			/**
			 * This has the default behaviour of storing the memory pool hosting the resource, however each derived class
			 * MUST provide its own implementation that also uses this.
			 * 
			 * @param memoryPool the memory pool hosting the resource
			 * @param startingOffset the offset from the start of the memory pool
			 */
			virtual void bindMemory(MemoryPool* memoryPool, VkDeviceSize startingOffset) noexcept;

		private:
			mutable std::unique_ptr<VkMemoryRequirements> mMemoryRequirements;

			MemoryPool* mUsedMemoryPool;

			VkDeviceSize mStartingOffset;
		};
	}
}