#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Fence :
			virtual public DeviceOwned {

		public:
			Fence(Device* const device, VkFence&& fence) noexcept;

			Fence(const Fence&) = delete;

			Fence(Fence&&) = delete;

			Fence& operator=(const Fence&) = delete;

			~Fence() override;

			const VkFence& getNativeFanceHandle() const noexcept;

			void reset() noexcept;

		private:
			VkFence mFence;
		};
	}
}