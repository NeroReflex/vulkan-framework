#pragma once

#include "ShaderLayoutBinding.h"
#include "ImageInterface.h"
#include "Sampler.h"
#include "Buffer.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class DescriptorPool;

		class DescriptorSet {

		public:
			DescriptorSet(DescriptorPool* const pool, VkDescriptorSet&& descriptorPool) noexcept;

			DescriptorSet(const DescriptorSet&) = delete;

			DescriptorSet(DescriptorSet&&) = delete;

			DescriptorSet& operator=(const DescriptorSet&) = delete;

			~DescriptorSet();

			DescriptorPool* getParentDescriptorPool() const noexcept;

			const VkDescriptorSet& getNativeDescriptorSetHandle() const noexcept;

			void bindStorageBuffers(uint32_t firstLayoutId, const std::vector<const Buffer*>& buffers) const noexcept;

			void bindUniformBuffers(uint32_t firstLayoutId, const std::vector<const Buffer*>& buffers) const noexcept;

			/**
			 * Update a set of contiguous image bindings starting from the given binding point.
			 *
			 * @param firstLayoutId the number of the fisrst layout
			 * @param images images to bind from the first binding point onward
			 */
			void bindStorageImages(uint32_t firstLayoutId, std::vector<std::tuple<VkImageLayout, const ImageView*>> images) const noexcept;

			void bindCombinedImageSampler(uint32_t firstLayoutId, std::vector<std::tuple<VkImageLayout, const ImageView*, const Sampler*>> CombinedSamplers) const noexcept;

			/**
			 * Update a texture array descriptor set.
			 *
			 * @param firstLayoutId the layout location of the uniform
			 * @param images images to bind to the array of textures
			 */
			void bindSampledImages(uint32_t firstLayoutId, std::vector<std::tuple<VkImageLayout, const ImageView*>> images) const noexcept;

			void bindInputAttachment(uint32_t inputAttachmentIndex, const ImageView* attachment) const noexcept;

			void bindSampler(uint32_t firstLayoutId, const VulkanFramework::Sampler* const sampler) const noexcept;

		private:
			DescriptorPool* const mParentDescriptorPool;

			VkDescriptorSet mDescriptorSet;
		};
	}
}