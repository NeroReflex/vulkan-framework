#pragma once

#include "VulkanFramework.h"
#include "Utils/SurfaceDimensions.h"
#include "GraphicPipeline.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class ImageInterface;

		class Framebuffer;

		class ImageView {

			friend class Framebuffer;

		public:
			enum class ViewType {
				Image1D,
				Image2D,
				Image3D,
				CubeMap,
				Image1DArray,
				Image2DArray,
				CubeMapArray,
			};

			enum class ViewColorMapping {
				rgba_rgba,
				bgra_rgba
			};

			ImageView(ImageInterface* image, VkImageView&& imageView) noexcept;

			ImageView(const ImageView&) = delete;

			ImageView(ImageView&&) = delete;

			ImageView& operator=(const ImageView&) = delete;

			virtual ~ImageView();

			const ImageInterface* getParentImage() const noexcept;

			const VkImageView& getNativeImageViewHandle() const noexcept;

		private:
			ImageInterface* mParentImage;

			VkImageView mImageView;
		};
	}
}
