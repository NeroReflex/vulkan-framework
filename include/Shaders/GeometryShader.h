#pragma once

#include "Shaders/Shader.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Shaders {

			class GeometryShader :
				public Shader {

			public:
				inline GeometryShader(
					Device* device,
					std::vector<VkDescriptorSetLayoutBinding>&& bindings,
					std::vector<VkPushConstantRange>&& pushConstants,
					VkShaderModule&& module
				) noexcept
					: DeviceOwned(device), Shader(device, Shaders::ShaderType::Geometry, std::move(bindings), std::move(pushConstants), std::move(module)) {};

				GeometryShader() = delete;

				GeometryShader(const GeometryShader&) = delete;

				GeometryShader(GeometryShader&&) = delete;

				GeometryShader& operator=(const GeometryShader&) = delete;

				~GeometryShader() override = default;
			};

		}
	}
}