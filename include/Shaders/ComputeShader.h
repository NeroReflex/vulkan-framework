#pragma once

#include "Shaders/Shader.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Shaders {

			class ComputeShader :
				public Shader {

			public:
				inline ComputeShader(
					Device* device,
					std::vector<VkDescriptorSetLayoutBinding>&& bindings,
					std::vector<VkPushConstantRange>&& pushConstants,
					VkShaderModule&& module
				) noexcept
					: DeviceOwned(device), Shader(device, Shaders::ShaderType::Compute, std::move(bindings), std::move(pushConstants), std::move(module)) {};

				ComputeShader() = delete;

				ComputeShader(const ComputeShader&) = delete;

				ComputeShader(ComputeShader&&) = delete;

				ComputeShader& operator=(const ComputeShader&) = delete;

				~ComputeShader() override = default;
			};

		}
	}
}