#pragma once

#include "Shaders/Shader.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Shaders {

			class FragmentShader :
				public Shader {

			public:
				inline FragmentShader(
					Device* device,
					std::vector<VkDescriptorSetLayoutBinding>&& bindings,
					std::vector<VkPushConstantRange>&& pushConstants, 
					VkShaderModule&& module
				) noexcept
					: DeviceOwned(device), Shader(device, Shaders::ShaderType::Fragment, std::move(bindings), std::move(pushConstants), std::move(module)) {};

				FragmentShader() = delete;

				FragmentShader(const FragmentShader&) = delete;

				FragmentShader(FragmentShader&&) = delete;

				FragmentShader& operator=(const FragmentShader&) = delete;

				~FragmentShader() override = default;
			};

		}
	}
}