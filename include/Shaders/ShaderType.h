#pragma once

#include "DeviceOwned.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Shaders {

			enum ShaderType {
				Vertex = 0,
				Geometry = 1,
				Fragment = 2,
				Compute = 8,
			};

		}
	}
}