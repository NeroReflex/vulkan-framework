#pragma once

#include "Shaders/Shader.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Shaders {

			class VertexShader :
				public Shader {

			public:
				inline VertexShader(
					Device* device,
					std::vector<VkDescriptorSetLayoutBinding>&& bindings,
					std::vector<VkPushConstantRange>&& pushConstants,
					VkShaderModule&& module
				) noexcept
					: DeviceOwned(device), Shader(device, Shaders::ShaderType::Vertex, std::move(bindings), std::move(pushConstants), std::move(module)) {};

				VertexShader() = delete;

				VertexShader(const VertexShader&) = delete;

				VertexShader(VertexShader&&) = delete;

				VertexShader& operator=(const VertexShader&) = delete;

				~VertexShader() override = default;
			};

		}
	}
}