#pragma once

#include "DeviceOwned.h"
#include "Shaders/ShaderType.h"
#include "ShaderLayoutBinding.h"

namespace NeroReflex {
	namespace VulkanFramework {
		namespace Shaders {

			class Shader :
				virtual public DeviceOwned {

			public:
				Shader() = delete;

				Shader(const Shader&) = delete;

				Shader(Shader&&) = delete;

				Shader& operator=(const Shader&) = delete;

				~Shader() override;

				const VkShaderModule& getNativeShaderModuleHandle() const noexcept;

				const std::vector<VkDescriptorSetLayoutBinding>& getNativeShaderBindings() const noexcept;

				const std::vector<VkPushConstantRange>& getNativeShaderPushConstants() const noexcept;

				const ShaderType& getType() const noexcept;

				VkShaderStageFlagBits getStageFlagBits() const noexcept;

			protected:
				Shader(Device* device, ShaderType type, std::vector<VkDescriptorSetLayoutBinding>&& bindings, std::vector<VkPushConstantRange>&& pushConstants, VkShaderModule&& module) noexcept;

			private:
				ShaderType mShaderType;

				std::vector<VkDescriptorSetLayoutBinding> mSpecializedBindings;

				std::vector<VkPushConstantRange> mSpecializedPushContants;

				VkShaderModule mShaderModule;
			};

		}
	}
}