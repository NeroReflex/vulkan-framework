#pragma once

#include "QueueFamily.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class Queue {

		public:
			Queue(QueueFamily* const queueFamily, VkQueue&& queue) noexcept;

			~Queue();

			QueueFamily* getParentQueueFamily() const noexcept;

			const VkQueue& getNativeQueueHandle() const noexcept;

		private:
			QueueFamily* const mQueueFamily;

			VkQueue mQueue;
		};
	}
}