#pragma once

#include "QueueFamily.h"

namespace NeroReflex {
	namespace VulkanFramework {

		class InstanceOwned;
		class Device;

		class Instance {

		public:

			enum APIVersion {
#if defined (VK_API_VERSION_1_0)
				Version_1_0,
#endif

#if defined (VK_API_VERSION_1_1)
				Version_1_1,
#endif


#if defined (VK_API_VERSION_1_2)
				Version_1_2
#endif
			};

			static const float defaultQueuePriority;

			Instance(
				const std::vector<std::string>& instanceExtensions = std::vector<std::string>(),
				const std::string& engineName = "No Engine",
				const std::string& appName = "Hello World",
				const APIVersion& version = APIVersion::Version_1_0
			) noexcept;

			Instance(Instance&) = delete;

			Instance& operator=(Instance&) = delete;

			~Instance();

			VkSurfaceKHR createSurface(const std::function<VkSurfaceKHR()>& fn) const noexcept;

			Device* openDevice(
				std::vector<QueueFamily::ConcreteQueueFamilyDescriptor> queueDescriptors,
				const std::vector<std::string>& deviceExtensions = std::vector<std::string>(),
				const std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>& getPhysicalDevicePresentationSupport = std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>([](VkInstance, VkPhysicalDevice, uint32_t) {  return VK_FALSE; })
			) noexcept;

			static std::vector<VkExtensionProperties> getAllSupportedExtensions() noexcept;

			const VkInstance& getNativeInstanceHandle() const noexcept;

			void requireInstanceExtension(const char* extName) noexcept;

		private:
			bool corresponds(
				const std::vector<QueueFamily::QueueFamilySupportedOperationType>& operations,
				VkQueueFamilyProperties queueFamily,
				VkPhysicalDevice device,
				uint32_t familyIndex,
				uint32_t maxQueues,
				const std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>& getPhysicalDevicePresentationSupport
			) const noexcept;

			// TODO: implement the usage of this thing!
			VkAllocationCallbacks mCustomAllocator;

			std::string mEngineName;

			std::string mApplicationName;

#if defined(VULKAN_ENABLE_VALIDATION_LAYERS) 
			std::vector<const char*> mValidationLayers;
#endif
			VkInstance mInstance;

			std::vector<const char*> mInstanceExtensions;

			std::vector<std::unique_ptr<InstanceOwned>> mObjectsCollection;
		};
	}
}