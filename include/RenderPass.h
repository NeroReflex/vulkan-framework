#pragma once

#include "DeviceOwned.h"
#include "CommandBuffer.h"

namespace NeroReflex {
	namespace VulkanFramework {

		/*
		 * A single render pass can consist of multiple subpasses.
		 * Subpasses are subsequent rendering operations that depend on the contents of framebuffers in previous passes,
		 * for example a sequence of post-processing effects that are applied one after another.
		 * If you group these rendering operations into one render pass,
		 * then Vulkan is able to reorder the operations and conserve memory bandwidth for possibly better performance.
		 * 
		 * Every subpass references one or more attachments.
		 */
		class RenderPass :
			virtual public DeviceOwned {

		public:

			/**
			 * Represents the definition of a render subpass.
			 *
			 * The data contained in an object of this class is tightly coupled with the relative renderpass declaration.
			 */
			class RenderSubPass {

			public:

				/**
				 * Constructs a renderpass from indices of attachments of a renderpass.
				 *
				 * @param depthStencilAttachmentIndeces indices of depth stencil attachments
				 * @param colorAttachmentIndeces indices of color attachments (the 0-based position of this vector will become the layout (location = ?) out fragColor)
				 */
				RenderSubPass(
					const std::vector<uint32_t>& inputColorAttachmentsIndeces = std::vector<uint32_t>({}),
					const std::vector<uint32_t>& colorAttachmentIndeces = std::vector<uint32_t>({}),
					const std::optional<uint32_t> & depthStencilAttachmentIndeces = std::optional<uint32_t>()
				) noexcept;

				const std::vector<uint32_t>& getInputColorAttachmentIndeces() const noexcept;

				const std::vector<uint32_t>& getColorAttachmentIndeces() const noexcept;

				const std::optional<uint32_t>& getDepthStencilAttachmentIndex() const noexcept;

			private:
				std::vector<uint32_t> mInputColorAttachmentsIndeces;

				std::vector<uint32_t> mColorAttachmentIndeces;

				std::optional<uint32_t> mDepthStencilAttachmentIndex;
			};

			RenderPass(
				Device* const,
				VkRenderPass&& renderPass,
				const std::vector<RenderPass::RenderSubPass>& subpasses,
				const std::vector<VkSubpassDependency>& dependencies,
				VkSampleCountFlagBits sampleNumber
			) noexcept;

			RenderPass(const RenderPass&) = delete;

			RenderPass(RenderPass&&) = delete;

			RenderPass& operator=(const RenderPass&) = delete;

			~RenderPass() override;

			const VkRenderPass& getNativeRenderPassHandle() const noexcept;

			const VkSampleCountFlagBits& getSampleCount() const noexcept;

			uint32_t getSubpassesCount() const noexcept;

			const RenderSubPass& getSubpassByIndex(uint32_t index) const noexcept;

		private:
			std::vector<RenderPass::RenderSubPass> mRenderSubPasses;

			std::vector<VkSubpassDependency> mSubpassesDependencies;

			VkRenderPass mRenderPass;

			VkSampleCountFlagBits mSampleCount;
		};
	}
}
