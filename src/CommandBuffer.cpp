#include "CommandBuffer.h"
#include "CommandPool.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

CommandBuffer::CommandBuffer(CommandPool* const parentCommandPool, VkCommandBuffer&& commandBuffer) noexcept
	: mParentCommandPool(parentCommandPool),
	mCommandBuffer(commandBuffer) {}

CommandBuffer::~CommandBuffer() {}

void CommandBuffer::registerCommands(std::function<void(const VkCommandBuffer& commandBuffer)> fn) noexcept {
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.pNext = nullptr;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT; // the buffer is only submitted and used once in this application.
	VK_CHECK_RESULT(vkBeginCommandBuffer(mCommandBuffer, &beginInfo)); // start recording commands.

	fn(mCommandBuffer);

	VK_CHECK_RESULT(vkEndCommandBuffer(mCommandBuffer)); // end recording commands.
}

void CommandBuffer::registerCommands(std::function<void(CommandBuffer * commandBuffer)> fn) noexcept {
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.pNext = nullptr;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT; // the buffer is only submitted and used once in this application.
	VK_CHECK_RESULT(vkBeginCommandBuffer(mCommandBuffer, &beginInfo)); // start recording commands.

	fn(this);

	VK_CHECK_RESULT(vkEndCommandBuffer(mCommandBuffer)); // end recording commands.
}

void CommandBuffer::submit(const Queue* const queue, const Fence* const fence, std::vector<const Semaphore*> signalSemaphores, std::vector<std::tuple<const Semaphore*, VkPipelineStageFlags>> waitSemaphores) noexcept
{
	size_t i = 0;
	std::vector<VkSemaphore> waitNativeSemaphores(waitSemaphores.size());
	std::vector<VkPipelineStageFlags> waitNativeStage(waitSemaphores.size());
	for (i = 0; i < waitSemaphores.size(); ++i) {
		waitNativeSemaphores[i] = std::get<0>(waitSemaphores[i])->getNativeSemaphoreHandle();
		waitNativeStage[i] = std::get<1>(waitSemaphores[i]);
	}
	
	std::vector<VkSemaphore> signalNativeSemaphores(signalSemaphores.size());
	for (i = 0; i < signalSemaphores.size(); ++i) {
		signalNativeSemaphores[i] = signalSemaphores[i]->getNativeSemaphoreHandle();
	}

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.pNext = nullptr;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &mCommandBuffer;
	submitInfo.waitSemaphoreCount = static_cast<uint32_t>(waitNativeSemaphores.size());
	submitInfo.pWaitSemaphores = waitNativeSemaphores.data();
	submitInfo.signalSemaphoreCount = static_cast<uint32_t>(signalNativeSemaphores.size());
	submitInfo.pSignalSemaphores = signalNativeSemaphores.data();
	submitInfo.pWaitDstStageMask = waitNativeStage.data();

	VK_CHECK_RESULT(vkQueueSubmit(queue->getNativeQueueHandle(), 1, &submitInfo, fence != nullptr ? fence->getNativeFanceHandle() : VK_NULL_HANDLE));
}
