#include "DeviceOwned.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

DeviceOwned::DeviceOwned(Device* device) noexcept : mOwningDevice(device) {
	getParentDevice()->mOwnedObjects.emplace(std::pair<uintptr_t, std::unique_ptr<DeviceOwned>>(uintptr_t(this), this));
}

DeviceOwned::~DeviceOwned() {
	// To avoid double-delete bugs that are very difficult to resolve this object won't self-unregister from owning device.
	
	// When owning device gets destroyed its internal list of owned devices will be destroyed, and this object will too.

	// I a destruction has to happen during run-time the developer will call destroy(this) on the owning device, that will
	// remove this pointer from its internal list, trigghering the delete call
}

Device* DeviceOwned::getParentDevice() const noexcept {
	return mOwningDevice;
}