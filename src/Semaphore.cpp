#include "Semaphore.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Semaphore::Semaphore(Device* const device, VkSemaphore&& semaphore) noexcept
	: DeviceOwned(device), mSemaphore(std::move(semaphore)) {}

Semaphore::~Semaphore() {
	vkDestroySemaphore(getParentDevice()->getNativeDeviceHandle(), mSemaphore, nullptr);
}

const VkSemaphore& Semaphore::getNativeSemaphoreHandle() const noexcept {
	return mSemaphore;
}
