#include "Utils/Rasterizer.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;
using namespace NeroReflex::VulkanFramework::Utils; 

const float Rasterizer::defaultLineWidth = 1.0;

Rasterizer::Rasterizer(
	VkCullModeFlags cullMode,
	VkFrontFace mFrontFace,
	PolygonModeType polygonModeType,
	float lineWidth,
	bool enableDepthClamp,
	bool enableDepthBias,
	float mDepthBiasCostantFactor,
	float mDepthBiasClamp,
	float mDepthBiasSlopeFactor,
	const std::optional<VkConservativeRasterizationModeEXT>& conservativeRasterizationSettings
) noexcept {

	mConservativeRasterizationState.pNext = nullptr;
	mConservativeRasterizationState.flags = 0; // Reserved for  future usage
	if (conservativeRasterizationSettings.has_value())
		mConservativeRasterizationState.conservativeRasterizationMode = conservativeRasterizationSettings.value();

	mCreateInfo.pNext = conservativeRasterizationSettings.has_value() ? reinterpret_cast<const void*>(&mConservativeRasterizationState) : nullptr;
	mCreateInfo.flags = 0; // Flags must be zero on the current vulkan spec
	mCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	mCreateInfo.rasterizerDiscardEnable = VK_FALSE; // Setting this fo VK_TRUE the geometry never passes through the rasterizer stage, disabling every output!

	mCreateInfo.depthClampEnable = enableDepthClamp ? VK_TRUE : VK_FALSE; // Setting this to VK_TRUE requires a GPU extension
	mCreateInfo.polygonMode = VK_POLYGON_MODE_FILL; // Setting this to VK_POLYGON_MODE_LINE or VK_POLYGON_MODE_POINT requires a GPU extension
	mCreateInfo.lineWidth = lineWidth; // Setting to something bigger requires the wideLines GPU extension
	mCreateInfo.cullMode = cullMode;
	mCreateInfo.frontFace = mFrontFace;
	mCreateInfo.depthBiasEnable = enableDepthBias ? VK_TRUE : VK_FALSE;
	mCreateInfo.depthBiasConstantFactor = mDepthBiasCostantFactor; // Optional
	mCreateInfo.depthBiasClamp = mDepthBiasClamp; // Optional
	mCreateInfo.depthBiasSlopeFactor = mDepthBiasSlopeFactor; // Optional
}

const VkPipelineRasterizationStateCreateInfo& Rasterizer::getNativeRasterizationCreateInfoHandler() const noexcept {
	return mCreateInfo;
}