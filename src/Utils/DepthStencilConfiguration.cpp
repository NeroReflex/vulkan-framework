#include "Utils/DepthStencilConfiguration.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;
using namespace NeroReflex::VulkanFramework::Utils;

DepthStencilConfiguration::DepthStencilConfiguration(
	bool enableDepthTesting,
	bool depthWriteEnable,
	VkCompareOp depthCompareOperation,
	bool depthBoundsTestEnable,
	float minDepthBounds,
	float maxDepthBounds,
	bool stencilTestEnable,
	VkStencilOpState front,
	VkStencilOpState back
) noexcept
	: mEnableDepthTesting(enableDepthTesting),
	mDepthWriteEnable(depthWriteEnable),
	mDepthCompareOperation(depthCompareOperation),
	mDepthBoundsTestEnable(depthBoundsTestEnable),
	mStencilTestEnable(stencilTestEnable),
	mFront(front),
	mBack(back),
	mMinDepthBounds(minDepthBounds),
	mMaxDepthBounds(maxDepthBounds) {}

bool DepthStencilConfiguration::getEnableDepthTesting() const noexcept {
	return mEnableDepthTesting;
}

bool DepthStencilConfiguration::getDepthWriteEnable() const noexcept {
	return mDepthWriteEnable;
}

VkCompareOp DepthStencilConfiguration::getDepthCompareOperation() const noexcept {
	return mDepthCompareOperation;
}

bool DepthStencilConfiguration::getDepthBoundsTestEnable() const noexcept {
	return mDepthBoundsTestEnable;
}

bool DepthStencilConfiguration::getStencilTestEnable() const noexcept {
	return mStencilTestEnable;
}

VkStencilOpState DepthStencilConfiguration::getFront() const noexcept {
	return mFront;
}

VkStencilOpState DepthStencilConfiguration::getBack() const noexcept {
	return mBack;
}

float DepthStencilConfiguration::getMinDepthBounds() const noexcept {
	return mMinDepthBounds;
}

float DepthStencilConfiguration::getMaxDepthBounds() const noexcept {
	return mMaxDepthBounds;
}