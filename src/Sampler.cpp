#include "Sampler.h"

#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Sampler::Sampler(
	Device* const device,
	VkSampler&& sampler,
	Filtering magFilter,
	Filtering minFilter,
	Sampler::MipmapMode mipmapMode,
	float maxAnisotropy
) noexcept
	: DeviceOwned(device),
	mSampler(std::move(sampler)),
	mMagFilter(magFilter),
	mMinFilter(minFilter),
	mMipmapMode(mipmapMode),
	mMaxAnisotropy(maxAnisotropy) {

}

Sampler::~Sampler() {
	vkDestroySampler(getParentDevice()->getNativeDeviceHandle(), mSampler, nullptr);
}

const VkSampler& Sampler::getNativeSamplerHandle() const noexcept {
	return mSampler;
}

const Sampler::Filtering& Sampler::getMagFilter() const noexcept {
	return mMagFilter;
}

const Sampler::Filtering& Sampler::getMinFilter() const noexcept {
	return mMinFilter;
}

const Sampler::MipmapMode& Sampler::getMipmapMode() const noexcept {
	return mMipmapMode;
}

float Sampler::getMaxAnisotropy() const noexcept {
	return mMaxAnisotropy;
}

bool Sampler::isAnisotropicEnabled() const noexcept {
	return mMaxAnisotropy >= 1.0f;
}