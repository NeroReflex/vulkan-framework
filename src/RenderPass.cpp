#include "RenderPass.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

// =============== RENDER SUBPASS ===========================

RenderPass::RenderSubPass::RenderSubPass(
	const std::vector<uint32_t>& inputColorAttachmentsIndeces,
	const std::vector<uint32_t>& colorAttachmentIndeces,
	const std::optional<uint32_t>& depthStencilAttachmentIndex
) noexcept :
	mInputColorAttachmentsIndeces(inputColorAttachmentsIndeces),
	mColorAttachmentIndeces(colorAttachmentIndeces),
	mDepthStencilAttachmentIndex(depthStencilAttachmentIndex) {

}

const std::optional<uint32_t>& RenderPass::RenderSubPass::getDepthStencilAttachmentIndex() const noexcept {
	return mDepthStencilAttachmentIndex;
}

const std::vector<uint32_t>& RenderPass::RenderSubPass::getColorAttachmentIndeces() const noexcept {
	return mColorAttachmentIndeces;
}

const std::vector<uint32_t>& RenderPass::RenderSubPass::getInputColorAttachmentIndeces() const noexcept {
	return mInputColorAttachmentsIndeces;
}

// ================== RENDER PASS =============================

RenderPass::RenderPass(
	Device* const device,
	VkRenderPass&& renderPass,
	const std::vector<RenderPass::RenderSubPass>& subpasses,
	const std::vector<VkSubpassDependency>& dependencies,
	VkSampleCountFlagBits sampleNumber
) noexcept
	: DeviceOwned(device),
	mRenderSubPasses(subpasses),
	mSubpassesDependencies(dependencies),
	mRenderPass(std::move(renderPass)),
	mSampleCount(sampleNumber) {

}

RenderPass::~RenderPass() {
	vkDestroyRenderPass(getParentDevice()->getNativeDeviceHandle(), mRenderPass, nullptr);
}

const VkSampleCountFlagBits& RenderPass::getSampleCount() const noexcept {
	return mSampleCount;
}

uint32_t RenderPass::getSubpassesCount() const noexcept {
	return mRenderSubPasses.size();
}

const VkRenderPass& RenderPass::getNativeRenderPassHandle() const noexcept {
	return mRenderPass;
}

const RenderPass::RenderSubPass& RenderPass::getSubpassByIndex(uint32_t index) const noexcept {
	VULKAN_DBG_ASSERT( (index < this->getSubpassesCount()) );

	return mRenderSubPasses[index];
}