#include "SpaceRequiringResource.h"
#include "MemoryPool.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

SpaceRequiringResource::SpaceRequiringResource(Device* device) noexcept
	: DeviceOwned(device), mMemoryRequirements(nullptr), mUsedMemoryPool(nullptr), mStartingOffset(0) {}

SpaceRequiringResource::~SpaceRequiringResource() {
	if (mUsedMemoryPool) mUsedMemoryPool->free(this);
}

const VkMemoryRequirements& SpaceRequiringResource::getMemoryRequirements() const noexcept
{
	if (!mMemoryRequirements) {
		auto result = queryMemoryRequirements().release();
		mMemoryRequirements.reset(result);
	}
	
	return *(mMemoryRequirements.get());
}

const VkDeviceSize& SpaceRequiringResource::getAllocationOffset() const noexcept {
	return mStartingOffset;
}

MemoryPool* SpaceRequiringResource::getAllocationMemoryPool() noexcept {
	return mUsedMemoryPool;
}

void SpaceRequiringResource::bindMemory(MemoryPool* memoryPool, VkDeviceSize startingOffset) noexcept {
	mUsedMemoryPool = memoryPool;
	mStartingOffset = startingOffset;
}
