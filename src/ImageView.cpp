#include "ImageView.h"
#include "ImageInterface.h"
#include "Device.h"
#include "Framebuffer.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

ImageView::ImageView(ImageInterface* image, VkImageView&& imageView) noexcept
	: mParentImage(image),
	mImageView(std::move(imageView)) {}

ImageView::~ImageView() {
	vkDestroyImageView(getParentImage()->getParentDevice()->getNativeDeviceHandle(), mImageView, nullptr);

    // TODO: what if this imageview was used to create a framebuffer?

    // vkDestroyImageView(getParentImage()->getParentDevice()->getNativeDeviceHandle(), mImageView, nullptr);
}

const ImageInterface* ImageView::getParentImage() const noexcept {
	return mParentImage;
}

const VkImageView& ImageView::getNativeImageViewHandle() const noexcept {
	return mImageView;
}
