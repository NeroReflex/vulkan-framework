#include "VertexInputAttribute.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

VertexInputAttribute::VertexInputAttribute(uint32_t location, uint32_t offset, AttributeType type) noexcept
	: mLocation(std::move(location)),
	mOffset(std::move(offset)),
	mFormat(std::move(VertexInputAttribute::transformToVkFormat(type))) {}

VkFormat VertexInputAttribute::transformToVkFormat(AttributeType type) noexcept {
	return static_cast<VkFormat>(type);
}

VkVertexInputAttributeDescription VertexInputAttribute::generateNativeHandler(uint32_t binding) const noexcept {
	VkVertexInputAttributeDescription attributeDescription = {};
	attributeDescription.offset = mOffset;
	attributeDescription.format = mFormat;
	attributeDescription.binding = binding;
	attributeDescription.location = mLocation;

	return std::move(attributeDescription);
}