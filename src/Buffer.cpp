#include "Buffer.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Buffer::Buffer(Device* const device, VkBufferUsageFlags usage, VkDeviceSize size, VkBuffer&& buffer) noexcept
	: DeviceOwned(device),
	SpaceRequiringResource(device),
	mUsage(std::move(usage)),
	mSize(size),
	mBuffer(std::move(buffer)) {}

Buffer::~Buffer() {
	vkDestroyBuffer(getParentDevice()->getNativeDeviceHandle(), mBuffer, nullptr);
}

const VkBuffer& Buffer::getNativeBufferHandle() const noexcept {
	return mBuffer;
}

std::unique_ptr<VkMemoryRequirements> Buffer::queryMemoryRequirements() const noexcept {
	auto memRequirements = std::make_unique<VkMemoryRequirements>();
	vkGetBufferMemoryRequirements(getParentDevice()->getNativeDeviceHandle(), getNativeBufferHandle(), memRequirements.get());
	
	return std::move(memRequirements);
}

void Buffer::bindMemory(MemoryPool* memoryPool, VkDeviceSize startingOffset) noexcept {
	SpaceRequiringResource::bindMemory(memoryPool, startingOffset);
	
	VK_CHECK_RESULT(vkBindBufferMemory(memoryPool->getParentDevice()->getNativeDeviceHandle(), getNativeBufferHandle(), memoryPool->getNativeDeviceMemoryHandle(), startingOffset));
}

const VkBufferUsageFlags& Buffer::getBufferUsage() const noexcept {
	return mUsage;
}

const VkDeviceSize& Buffer::getBufferSize() const noexcept {
	return mSize;
}
