#include "VertexInputBinding.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

VertexInputBinding::VertexInputBinding(uint32_t stride, VkVertexInputRate inputRate, const std::vector<VertexInputAttribute>& attributes) noexcept
	: mStride(std::move(stride)),
	mInputRate(std::move(inputRate)),
	mAttributes(attributes) {}

std::tuple<VkVertexInputBindingDescription, std::vector<VkVertexInputAttributeDescription>> VertexInputBinding::generateNativeHandlers(uint32_t binding) const noexcept {
	VkVertexInputBindingDescription bindingDescriptor = {};
	bindingDescriptor.binding = binding;
	bindingDescriptor.inputRate = mInputRate;
	bindingDescriptor.stride = mStride;

	std::vector<VkVertexInputAttributeDescription> attributes;
	for (const auto& attribute : mAttributes)
		attributes.emplace_back(std::move(attribute.generateNativeHandler(binding)));

	return std::make_tuple<VkVertexInputBindingDescription, std::vector<VkVertexInputAttributeDescription>>(std::move(bindingDescriptor), std::move(attributes));
}
