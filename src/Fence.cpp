#include "Fence.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Fence::Fence(Device* const device, VkFence&& fence) noexcept
	: DeviceOwned(device), mFence(std::move(fence)) {}

Fence::~Fence() {
	vkDestroyFence(getParentDevice()->getNativeDeviceHandle(), mFence, nullptr);
}

const VkFence& Fence::getNativeFanceHandle() const noexcept {
	return mFence;
}

void Fence::reset() noexcept {
	vkResetFences(getParentDevice()->getNativeDeviceHandle(), 1, &mFence);
}
