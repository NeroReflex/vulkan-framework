#include "Swapchain.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Swapchain::Swapchain(Device* device, VkSwapchainKHR&& swapchain, VkFormat format, Utils::SurfaceDimensions surfaceDimensions) noexcept
    : DeviceOwned(device),
	DeviceMemoryBuffer(device),
    mSwapchain(swapchain),
	mFormat(format),
	mSurfaceDimensions(std::move(surfaceDimensions)) {

	uint32_t swapchainImagesCount;
	vkGetSwapchainImagesKHR(getParentDevice()->getNativeDeviceHandle(), mSwapchain, &swapchainImagesCount, nullptr);

	std::vector<VkImage> swapchainImages(swapchainImagesCount);
	vkGetSwapchainImagesKHR(getParentDevice()->getNativeDeviceHandle(), mSwapchain, &swapchainImagesCount, swapchainImages.data());

	for (size_t i = 0; i < swapchainImages.size(); ++i) {
		mImages.emplace_back(new SwapchainImage(getParentDevice(), this, std::move(swapchainImages[i])));
	}
}

Swapchain::~Swapchain() {
	vkDestroySwapchainKHR(getParentDevice()->getNativeDeviceHandle(), mSwapchain, nullptr);
}

const Utils::SurfaceDimensions& Swapchain::getSurfaceDimensions() const noexcept {
	return mSurfaceDimensions;
}

const VkFormat& Swapchain::getFormat() const noexcept {
	return mFormat;
}

size_t Swapchain::getImagesCount() const noexcept {
	return mImages.size();
}

const VkSwapchainKHR& Swapchain::getNativeSwapchainHandle() const noexcept {
	return mSwapchain;
}

SwapchainImage* Swapchain::getSwapchainImageByIndex(uint32_t index) const noexcept {
	VULKAN_DBG_ASSERT( (index < mImages.size()) );

	return mImages[index].get();
}

uint32_t Swapchain::acquireNextImage(const Semaphore* const semaphore, const Fence* const fence) const noexcept {
	uint32_t index;

	vkAcquireNextImageKHR(
		DeviceMemoryBuffer::getParentDevice()->getNativeDeviceHandle(),
		getNativeSwapchainHandle(),
		UINT64_MAX,
		semaphore != nullptr ? semaphore->getNativeSemaphoreHandle() : VK_NULL_HANDLE,
		fence != nullptr ? fence->getNativeFanceHandle() : VK_NULL_HANDLE,
		&index
	);

	return index;
}

void Swapchain::presentImageByIndex(const Queue* queue, uint32_t index, std::vector<const Semaphore*> waitSemaphores) const noexcept {
	size_t i;
	
	std::vector<VkSemaphore> waitNativeSemaphores(waitSemaphores.size());
	for (i = 0; i < waitSemaphores.size(); ++i) {
		waitNativeSemaphores[i] = waitSemaphores[i]->getNativeSemaphoreHandle();
	}
	
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.pNext = nullptr;
	presentInfo.waitSemaphoreCount = static_cast<uint32_t>(waitNativeSemaphores.size());
	presentInfo.pWaitSemaphores = waitNativeSemaphores.data();
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &mSwapchain;
	presentInfo.pImageIndices = &index;
	presentInfo.pResults = nullptr;
	
	vkQueuePresentKHR(queue->getNativeQueueHandle(), &presentInfo);

}
