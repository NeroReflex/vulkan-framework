#include "InstanceOwned.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

InstanceOwned::InstanceOwned(const Instance* instance) noexcept : mOwningInstance(instance) {}

InstanceOwned::~InstanceOwned() {}

const Instance* InstanceOwned::getParentInstance() const noexcept {
	return mOwningInstance;
}