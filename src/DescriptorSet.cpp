#include "DescriptorSet.h"
#include "DescriptorPool.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

DescriptorSet::DescriptorSet(DescriptorPool* const pool, VkDescriptorSet&& descriptorPool) noexcept
	: mParentDescriptorPool(pool),
	mDescriptorSet(std::move(descriptorPool)) {
	getParentDescriptorPool()->mAllocatedDescriptorSets.emplace(std::pair<uintptr_t, std::unique_ptr<DescriptorSet>>(uintptr_t(this), this));
}

DescriptorSet::~DescriptorSet() {
	const auto selfIt = getParentDescriptorPool()->mAllocatedDescriptorSets.find(uintptr_t(this));
	if (selfIt != getParentDescriptorPool()->mAllocatedDescriptorSets.cend()) {
		getParentDescriptorPool()->mAllocatedDescriptorSets.erase(selfIt);
	}

	vkFreeDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), getParentDescriptorPool()->getNativeDescriptorPoolHandle(), 1, &mDescriptorSet);
}

const VkDescriptorSet& DescriptorSet::getNativeDescriptorSetHandle() const noexcept {
	return mDescriptorSet;
}

DescriptorPool* DescriptorSet::getParentDescriptorPool() const noexcept {
	return mParentDescriptorPool;
}

void DescriptorSet::bindUniformBuffers(uint32_t firstLayoutId, const std::vector<const Buffer*>& buffers) const noexcept {
	std::vector<VkDescriptorBufferInfo> descriptors;

	for (const auto& buffer : buffers) {
		VULKAN_DBG_ASSERT( (buffer->getBufferUsage() & VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT) );

		VkDescriptorBufferInfo currentBufferDescriptor;
		currentBufferDescriptor.buffer = buffer->getNativeBufferHandle();
		currentBufferDescriptor.offset = 0;
		currentBufferDescriptor.range = VK_WHOLE_SIZE;

		descriptors.emplace_back(std::move(currentBufferDescriptor));
	}

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = firstLayoutId;
	writeDescriptorSet.descriptorCount = static_cast<uint32_t>(descriptors.size());
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeDescriptorSet.pBufferInfo = descriptors.data();

	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}

void DescriptorSet::bindCombinedImageSampler(uint32_t firstLayoutId, std::vector<std::tuple<VkImageLayout, const ImageView*, const Sampler*>> CombinedSamplers) const noexcept {
	std::vector<VkDescriptorImageInfo> descriptors;

	for (const auto& combinedSampler : CombinedSamplers) {
		VkDescriptorImageInfo textureDescriptor;
		// The image's view (images are never directly accessed by the shader, but rather through views defining subresources)
		textureDescriptor.imageView = std::get<1>(combinedSampler)->getNativeImageViewHandle();

		// The sampler (Telling the pipeline how to sample the texture, including repeat, border, etc.)
		textureDescriptor.sampler = std::get<2>(combinedSampler)->getNativeSamplerHandle();

		// The current layout of the image (Note: Should always fit the actual use, e.g. shader read)
		textureDescriptor.imageLayout = std::get<0>(combinedSampler);

		descriptors.emplace_back(std::move(textureDescriptor));
	}
	
	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = firstLayoutId;
	writeDescriptorSet.descriptorCount = static_cast<uint32_t>(descriptors.size());
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeDescriptorSet.pImageInfo = descriptors.data();

	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}

void DescriptorSet::bindStorageBuffers(uint32_t firstLayoutId, const std::vector<const Buffer*>& buffers) const noexcept {
	std::vector<VkDescriptorBufferInfo> descriptors;

	for (const auto& buffer : buffers) {
		VULKAN_DBG_ASSERT((buffer->getBufferUsage() & VK_BUFFER_USAGE_STORAGE_BUFFER_BIT));

		VkDescriptorBufferInfo currentBufferDescriptor;
		currentBufferDescriptor.buffer = buffer->getNativeBufferHandle();
		currentBufferDescriptor.offset = 0;
		currentBufferDescriptor.range = VK_WHOLE_SIZE;

		descriptors.emplace_back(std::move(currentBufferDescriptor));
	}

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = firstLayoutId;
	writeDescriptorSet.descriptorCount = static_cast<uint32_t>(descriptors.size());
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	writeDescriptorSet.pBufferInfo = descriptors.data();

	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}

void DescriptorSet::bindStorageImages(uint32_t firstLayoutId, std::vector<std::tuple<VkImageLayout, const ImageView*>> images) const noexcept {
	std::vector<VkDescriptorImageInfo> descriptors;
	
	for (const auto& image : images) {
		VkDescriptorImageInfo currentImageDescriptor;
		currentImageDescriptor.imageLayout = std::get<0>(image);

		const auto currentImgView = std::get<1>(image);
		if (currentImgView)
			currentImageDescriptor.imageView = currentImgView->getNativeImageViewHandle();

		descriptors.emplace_back(std::move(currentImageDescriptor));
	}

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = firstLayoutId;
	writeDescriptorSet.descriptorCount = static_cast<uint32_t>(descriptors.size());
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	writeDescriptorSet.pImageInfo = descriptors.data();
	
	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}

void DescriptorSet::bindSampledImages(uint32_t firstLayoutId, std::vector<std::tuple<VkImageLayout, const ImageView*>> images) const noexcept {
	std::vector<VkDescriptorImageInfo> descriptors;

	for (const auto& image : images) {
		VkDescriptorImageInfo currentImageDescriptor;
		currentImageDescriptor.imageLayout = std::get<0>(image);

		const auto currentImgView = std::get<1>(image);
		if (currentImgView)
			currentImageDescriptor.imageView = currentImgView->getNativeImageViewHandle();

		descriptors.emplace_back(std::move(currentImageDescriptor));
	}

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = firstLayoutId;
	writeDescriptorSet.descriptorCount = static_cast<uint32_t>(descriptors.size());
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
	writeDescriptorSet.pImageInfo = descriptors.data();

	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}

void DescriptorSet::bindSampler(uint32_t firstLayoutId, const VulkanFramework::Sampler* const sampler) const noexcept {
	VkDescriptorImageInfo samplerDescriptor = {};
	samplerDescriptor.sampler = sampler->getNativeSamplerHandle();

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = firstLayoutId;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
	writeDescriptorSet.pImageInfo = &samplerDescriptor;

	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}

void DescriptorSet::bindInputAttachment(uint32_t inputAttachmentIndex, const ImageView* attachment) const noexcept {
	VkDescriptorImageInfo samplerDescriptor = {};
	samplerDescriptor.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	samplerDescriptor.imageView = attachment->getNativeImageViewHandle();
	samplerDescriptor.sampler = VK_NULL_HANDLE;

	VkWriteDescriptorSet writeDescriptorSet = {};
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.pNext = nullptr;
	writeDescriptorSet.dstSet = mDescriptorSet;
	writeDescriptorSet.dstBinding = inputAttachmentIndex;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
	writeDescriptorSet.pImageInfo = &samplerDescriptor;

	vkUpdateDescriptorSets(getParentDescriptorPool()->getParentDevice()->getNativeDeviceHandle(), 1, &writeDescriptorSet, 0, NULL);
}