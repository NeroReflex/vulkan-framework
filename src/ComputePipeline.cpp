#include "ComputePipeline.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

ComputePipeline::ComputePipeline(Device* device, VkPipelineLayout&& pipelineLayout, VkDescriptorSetLayout&& descriptorSetLayout, VkPipeline&& pipeline) noexcept
	: DeviceOwned(device),
	Pipeline(device, Pipeline::PipelineType::Compute, std::move(pipelineLayout), std::move(descriptorSetLayout), std::move(pipeline)) {}