#include "Image.h"
#include "ImageView.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Image::Image(Device* device, ImageType type, VkFormat format, VkExtent3D extent, VkSampleCountFlagBits samples, uint32_t mipLevels, VkImage&& image) noexcept
	: DeviceOwned(device),
	SpaceRequiringResource(device),
	ImageInterface(device, std::move(image)),
	mType(type),
	mFormat(format),
	mMipLevels(mipLevels),
	mExtent(extent),
	mSamples(samples) {}

Image::~Image() {
	vkDestroyImage(getParentDevice()->getNativeDeviceHandle(), getNativeImageHandle(), nullptr);
}

std::unique_ptr<VkMemoryRequirements> Image::queryMemoryRequirements() const noexcept {
	auto memRequirements = std::make_unique<VkMemoryRequirements>();
	vkGetImageMemoryRequirements(getParentDevice()->getNativeDeviceHandle(), getNativeImageHandle(), memRequirements.get());

	return std::move(memRequirements);
}

void Image::bindMemory(MemoryPool* memoryPool, VkDeviceSize startingOffset) noexcept {
	SpaceRequiringResource::bindMemory(memoryPool, startingOffset);
	
	VK_CHECK_RESULT(vkBindImageMemory(memoryPool->getParentDevice()->getNativeDeviceHandle(), getNativeImageHandle(), memoryPool->getNativeDeviceMemoryHandle(), startingOffset));
}

ImageView* Image::createImageView(
	ImageView::ViewType type,
	std::optional<VkFormat> format,
	VkImageAspectFlagBits subrangeAspectBits,
	ImageView::ViewColorMapping swizzle,
	std::optional<uint32_t> subrangeBaseMipLevel,
	std::optional<uint32_t> subrangeLevelCount,
	uint32_t subrangeBaseArrayLayer,
	uint32_t subrangeLayerCount
) noexcept {
	VkImageViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.image = getNativeImageHandle();
	createInfo.format = (format.has_value()) ? format.value() : this->getFormat();
	createInfo.subresourceRange.aspectMask = subrangeAspectBits;
	createInfo.subresourceRange.baseMipLevel = subrangeBaseMipLevel.value_or(0);
	createInfo.subresourceRange.levelCount = subrangeLevelCount.value_or(this->getMipLevels());
	createInfo.subresourceRange.baseArrayLayer = subrangeBaseArrayLayer;
	createInfo.subresourceRange.layerCount = subrangeLayerCount;
	
	switch (type) {
	case ImageView::ViewType::Image1D:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
		break;

	case ImageView::ViewType::Image2D:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		break;

	case ImageView::ViewType::Image3D:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
		break;

	case ImageView::ViewType::CubeMap:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
		break;

	case ImageView::ViewType::Image1DArray:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_1D_ARRAY;
		break;

	case ImageView::ViewType::Image2DArray:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
		break;

	case ImageView::ViewType::CubeMapArray:
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
		break;

	default:
		VULKAN_DBG_ASSERT(false);
		break;
	}

	switch (swizzle) {
	case ImageView::ViewColorMapping::rgba_rgba:
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		break;

	case ImageView::ViewColorMapping::bgra_rgba:
		createInfo.components.r = VK_COMPONENT_SWIZZLE_B;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_G;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_R;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_A;
		break;

	default:
		VULKAN_DBG_ASSERT(false);
		break;
	}

	VkImageView imageView;
	VK_CHECK_RESULT(vkCreateImageView(getParentDevice()->getNativeDeviceHandle(), &createInfo, nullptr, &imageView));

	ImageView* created = new ImageView(this, std::move(imageView));

	mImageViews.emplace(std::pair<uintptr_t, std::unique_ptr<ImageView>>(uintptr_t(created), created));

	return created;
}

void Image::destroyImageView(ImageView* imgView) noexcept {
	uintptr_t idx = uintptr_t(imgView);

	const auto findResult = mImageViews.find(idx);

	if (findResult == mImageViews.cend()) {
		VULKAN_DBG_ASSERT(false);

		return;
	}

	mImageViews.erase(idx);
}

const VkFormat& Image::getFormat() const noexcept {
	return mFormat;
}

const VkExtent3D& Image::getExtent() const noexcept {
	return mExtent;
}

uint32_t Image::getMipLevels() const noexcept {
	return mMipLevels;
}

VkSampleCountFlagBits Image::getSamplesCount() const noexcept {
	return mSamples;
}