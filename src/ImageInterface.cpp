#include "ImageInterface.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

ImageInterface::ImageInterface(Device* device, VkImage&& image) noexcept
	: DeviceOwned(device),
	mImage(std::move(image)) {}

ImageInterface::~ImageInterface() {}

const VkImage& ImageInterface::getNativeImageHandle() const noexcept {
	return mImage;
}
