#include "Framebuffer.h"

#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

Framebuffer::Framebuffer(Device* const device, VkFramebuffer&& framebuffer, const std::vector<ImageView*>& attachedImageViews) noexcept
	: DeviceOwned(device),
	mFramebuffer(std::move(framebuffer)),
	mAttachedImageViews(attachedImageViews) {
	
}

Framebuffer::~Framebuffer() {
	vkDestroyFramebuffer(getParentDevice()->getNativeDeviceHandle(), mFramebuffer, nullptr);
}

const std::vector<ImageView*>& Framebuffer::getAttachmentImageViews() const noexcept {
	return mAttachedImageViews;
}

const VkFramebuffer& Framebuffer::getNativeFramebufferHandle() const noexcept {
	return mFramebuffer;
}



