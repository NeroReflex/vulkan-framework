#include "CommandPool.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

CommandPool::CommandPool(Device* const device, VkCommandPool&& commandPool) noexcept 
	: DeviceOwned(device),
	mCommandPool(std::move(commandPool)) {}

CommandPool::~CommandPool() {
	vkDestroyCommandPool(getParentDevice()->getNativeDeviceHandle(), mCommandPool, nullptr);
}

const VkCommandPool& CommandPool::getNativeCommandPoolHandle() const noexcept {
	return mCommandPool;
}

CommandBuffer* CommandPool::createCommandBuffer() noexcept {
	VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.pNext = nullptr;
	commandBufferAllocateInfo.commandPool = mCommandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = 1; // allocate a single command buffer. 

	VkCommandBuffer cmdBuffer;
	VK_CHECK_RESULT(vkAllocateCommandBuffers(getParentDevice()->getNativeDeviceHandle(), &commandBufferAllocateInfo, &cmdBuffer)); // allocate command buffer.

	CommandBuffer* buffer = new CommandBuffer(this, std::move(cmdBuffer));

	mRegisteredCommandBuffers.emplace_back(buffer);

	return buffer;
}

std::vector<CommandBuffer*> CommandPool::createCommandBuffers(uint32_t count) noexcept {
	VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.pNext = nullptr;
	commandBufferAllocateInfo.commandPool = mCommandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = count;

	std::vector<VkCommandBuffer> cmdBuffers(count);
	VK_CHECK_RESULT(vkAllocateCommandBuffers(getParentDevice()->getNativeDeviceHandle(), &commandBufferAllocateInfo, cmdBuffers.data())); // allocate command buffer.

	
	std::vector<CommandBuffer*> result(count);
	for (uint32_t i = 0; i < count; ++i) {
		result[i] = new CommandBuffer(this, std::move(cmdBuffers[i]));
		mRegisteredCommandBuffers.emplace_back(result[i]);
	}

	return result;
}