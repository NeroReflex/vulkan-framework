#include "GraphicPipeline.h"

#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

GraphicPipeline::GraphicPipeline(
	Device* device,
	VkPipelineLayout&& pipelineLayout,
	VkDescriptorSetLayout&& descriptorSetLayout,
	VkPipeline&& pipeline,
	Utils::Rasterizer&& rasterizer,
	Utils::DepthStencilConfiguration&& depthStencilConfig,
	Utils::SurfaceDimensions surfaceDimensions,
	const RenderPass* const renderPass
) noexcept
	: DeviceOwned(device),
	Pipeline(device, Pipeline::PipelineType::Compute, std::move(pipelineLayout), std::move(descriptorSetLayout), std::move(pipeline)),
	mRasterizer(std::move(rasterizer)),
	mSurfaceDimensions(std::move(surfaceDimensions)),
	mRenderPass(renderPass) {}

GraphicPipeline::~GraphicPipeline() {}

const RenderPass* GraphicPipeline::getRenderPass() const noexcept {
	return mRenderPass;
}

const Utils::Rasterizer& GraphicPipeline::getRasterizer() const noexcept {
	return mRasterizer;
}

const Utils::DepthStencilConfiguration& GraphicPipeline::getDepthStencilConfiguration() const noexcept {
	return mDepthStencilConfig;
}