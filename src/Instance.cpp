#include "Instance.h"
#include "InstanceOwned.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

const float Instance::defaultQueuePriority = 1.0;

Instance::Instance(
	const std::vector<std::string>& instanceExtensions,
	const std::string& engineName,
	const std::string& appName,
	const APIVersion& version
) noexcept
	: mEngineName(engineName),
	mApplicationName(appName) {


		for (const auto& instanceExtension: instanceExtensions) {
			this->requireInstanceExtension(instanceExtension.c_str());
		}

		uint32_t requiredVersion = 0;

		switch (version) {
#if defined (VK_API_VERSION_1_0)
			case Version_1_0:
				requiredVersion = VK_API_VERSION_1_0;
				break;
#endif
			
#if defined (VK_API_VERSION_1_1)
			case Version_1_1:
				requiredVersion = VK_API_VERSION_1_1;
				break;
#endif

#if defined (VK_API_VERSION_1_2)
			case Version_1_2:
				requiredVersion = VK_API_VERSION_1_2;
				break;
#endif
		}

		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = mApplicationName.c_str();
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 1);
		appInfo.pEngineName = mEngineName.c_str();
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 1);
		appInfo.apiVersion = requiredVersion;

#if defined(VULKAN_ENABLE_VALIDATION_LAYERS) 
		mValidationLayers.emplace_back("VK_LAYER_KHRONOS_validation");
#endif

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.enabledExtensionCount = static_cast<uint32_t>(mInstanceExtensions.size());
		createInfo.ppEnabledExtensionNames = mInstanceExtensions.data();
#if defined(VULKAN_ENABLE_VALIDATION_LAYERS) 
		{
			createInfo.enabledLayerCount = static_cast<uint32_t>(mValidationLayers.size());
			createInfo.ppEnabledLayerNames = mValidationLayers.data();
		}
#else
		{ // Disable validation layers
			createInfo.enabledLayerCount = 0;
		}
#endif

		VK_CHECK_RESULT(vkCreateInstance(&createInfo, nullptr, &mInstance));
}

Instance::~Instance() {
	for (auto& ownedObj : mObjectsCollection)
		ownedObj.reset();

	vkDestroyInstance(mInstance, nullptr);

	for (uint32_t j = 0;  j < mInstanceExtensions.size(); ++j) delete[] mInstanceExtensions[j];
}

void Instance::requireInstanceExtension(const char* extName) noexcept {
	const auto extNameLen = std::strlen(extName);

	char* extensionName = new char[extNameLen + 1];
	std::memcpy(reinterpret_cast<void*>(extensionName), reinterpret_cast<const void*>(extName), extNameLen);
	extensionName[extNameLen] = 0x00;

	mInstanceExtensions.emplace_back(const_cast<const char*>(extensionName));
}

std::vector<VkExtensionProperties> Instance::getAllSupportedExtensions() noexcept {
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> extensions(extensionCount);

	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

	return extensions;
}

const VkInstance& Instance::getNativeInstanceHandle() const noexcept {
	return mInstance;
}

bool Instance::corresponds(
	const std::vector<QueueFamily::QueueFamilySupportedOperationType>& operations,
	VkQueueFamilyProperties queueFamily,
	VkPhysicalDevice device,
	uint32_t familyIndex,
	uint32_t maxQueues,
	const std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>& getPhysicalDevicePresentationSupport
) const noexcept {
	bool featureRequested = true;
	bool featureFound = false;

	if (maxQueues > queueFamily.queueCount) return false;
	
	auto opIt = std::find(operations.cbegin(), operations.cend(), QueueFamily::QueueFamilySupportedOperationType::Transfer);
	featureRequested = (opIt != operations.cend());
	featureFound = queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT;
	if (featureFound != featureRequested) return false;
	
	opIt = std::find(operations.cbegin(), operations.cend(), QueueFamily::QueueFamilySupportedOperationType::Compute);
	featureRequested = (opIt != operations.cend());
	featureFound = queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT;
	if (featureFound != featureRequested) return false;
	
	opIt = std::find(operations.cbegin(), operations.cend(), QueueFamily::QueueFamilySupportedOperationType::Graphics);
	featureRequested = (opIt != operations.cend());
	featureFound = queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT;
	if (featureFound != featureRequested) return false;
	
	opIt = std::find(operations.cbegin(), operations.cend(), QueueFamily::QueueFamilySupportedOperationType::Present);
	featureRequested = (opIt != operations.cend());
	if ((featureRequested) && (!getPhysicalDevicePresentationSupport(mInstance, device, familyIndex))) return false;
	
	return true;
}

Device* Instance::openDevice(
	std::vector<QueueFamily::ConcreteQueueFamilyDescriptor> queueDescriptors,
	const std::vector<std::string>& deviceExtensions,
	const std::function<VkBool32(VkInstance, VkPhysicalDevice, uint32_t)>& getPhysicalDevicePresentationSupport
) noexcept {
	VULKAN_DBG_ASSERT((queueDescriptors.size() > 0));

	std::vector<const char*> currentDeviceExtensions;

	for (const auto& deviceExt: deviceExtensions) {
		currentDeviceExtensions.push_back(deviceExt.c_str());
	}

	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(mInstance, &deviceCount, nullptr);

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(mInstance, &deviceCount, devices.data());

	uint64_t bestPhysicalDeviceScore = 0;
	VkPhysicalDevice bestPhysicalDevice = VK_NULL_HANDLE;
	VkPhysicalDeviceFeatures bestPhysicalDeviceFeatures;

	for (const VkPhysicalDevice& physicalDevice : devices) {
		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);

		uint64_t MSBytes = 0;
		
		switch (deviceProperties.deviceType) {
			case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
				MSBytes = 0xC000000000000000;
				break;

			case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
				MSBytes = 0x8000000000000000;

			case VK_PHYSICAL_DEVICE_TYPE_CPU:
				MSBytes = 0x4000000000000000;

			default:
				MSBytes = 0x0000000000000000;
		}

		uint64_t currentScore = MSBytes | deviceProperties.limits.maxImageDimension3D;

		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU) { // Exclude inadequate devices
			currentScore = 0;
		}

		if (bestPhysicalDeviceScore < currentScore) {
			bestPhysicalDevice = physicalDevice;
			bestPhysicalDeviceFeatures = deviceFeatures;
		}

	}

	VULKAN_DBG_ASSERT((bestPhysicalDevice != VK_NULL_HANDLE));

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(bestPhysicalDevice, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(bestPhysicalDevice, &queueFamilyCount, queueFamilies.data());

	const auto requestedDescriptors = queueDescriptors;
	
	std::vector<VkDeviceQueueCreateInfo> selectedQueues /*(requestedDescriptors.size())*/ ;
	std::vector<std::tuple<QueueFamily::ConcreteQueueFamilyDescriptor, uint32_t>> requiredQueueFamilyCollection;

	while (!queueDescriptors.empty()) {
		const auto currentDescriptorIt = queueDescriptors.cbegin();
		
		uint32_t familyIndex = 0;
		for (const auto& queueFamily : queueFamilies) {

			VULKAN_DBG_ASSERT((currentDescriptorIt->maxQueues > 0));

			if (corresponds(currentDescriptorIt->supportedOperations, queueFamily, bestPhysicalDevice, familyIndex, currentDescriptorIt->maxQueues, getPhysicalDevicePresentationSupport)) {
				VkDeviceQueueCreateInfo queueCreateInfo = {};
				queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
				queueCreateInfo.queueFamilyIndex = familyIndex;
				queueCreateInfo.queueCount = currentDescriptorIt->maxQueues;
				queueCreateInfo.pQueuePriorities = &defaultQueuePriority;

				selectedQueues.emplace_back(std::move(queueCreateInfo));

				requiredQueueFamilyCollection.emplace_back(std::make_tuple(*currentDescriptorIt, familyIndex));

				break; // found a suitable queue. Stop the search.
			}
			
			familyIndex++;
		}
		
		queueDescriptors.erase(currentDescriptorIt);
	}

	VULKAN_DBG_ASSERT(requestedDescriptors.size() == selectedQueues.size());

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pQueueCreateInfos = selectedQueues.data(); // not <= 
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(selectedQueues.size());
	createInfo.pEnabledFeatures = &bestPhysicalDeviceFeatures;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(currentDeviceExtensions.size());
	createInfo.ppEnabledExtensionNames = currentDeviceExtensions.data();

	/*
	Previous implementations of Vulkan made a distinction between instance and device specific validation layers, but this is no longer the case.
	That means that the enabledLayerCount and ppEnabledLayerNames fields of VkDeviceCreateInfo are ignored by up-to-date implementations.
	However, it is still a good idea to set them anyway to be compatible with older implementations:
	*/
#if defined(VULKAN_ENABLE_VALIDATION_LAYERS) 
	{
		createInfo.enabledLayerCount = static_cast<uint32_t>(mValidationLayers.size());
		createInfo.ppEnabledLayerNames = mValidationLayers.data();
	}
#else
	{
		createInfo.enabledLayerCount = 0;
	}
#endif

	VkDevice newDevice;

	const auto device_creation_res = vkCreateDevice(bestPhysicalDevice, &createInfo, nullptr, &newDevice);
	VK_CHECK_RESULT(device_creation_res);

	Device* managedDeviceHandle = new Device(
		this,
		std::move(bestPhysicalDevice),
		std::move(newDevice),
		std::move(requiredQueueFamilyCollection)
	);

	mObjectsCollection.emplace_back(managedDeviceHandle);

	return managedDeviceHandle;
}

VkSurfaceKHR Instance::createSurface(const std::function<VkSurfaceKHR()>& fn) const noexcept {
	return fn();
}