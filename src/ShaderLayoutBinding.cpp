#include "ShaderLayoutBinding.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

ShaderLayoutBinding::ShaderLayoutBinding(
	const std::vector<BindingDescriptor>& descriptors,
	const std::vector<PushConstantDescriptor>& pushConstants
) noexcept
	: mDescriptors(),
	mPushConstants() {
	for (const auto& desc : descriptors)
		insert(desc);

	for (const auto& pushConstant : pushConstants)
		insertPushConstant(pushConstant);
}

ShaderLayoutBinding::ShaderLayoutBinding(const ShaderLayoutBinding& src) noexcept
	: mDescriptors(src.mDescriptors) {}

ShaderLayoutBinding::ShaderLayoutBinding(ShaderLayoutBinding&& src) noexcept
	: mDescriptors(std::move(src.mDescriptors)) {}

ShaderLayoutBinding& ShaderLayoutBinding::operator=(const ShaderLayoutBinding& src) noexcept {
	if (this != &src) {
		mDescriptors = src.mDescriptors;
	}

	return *this;
}

ShaderLayoutBinding::~ShaderLayoutBinding() {}

void ShaderLayoutBinding::insertPushConstant(const PushConstantDescriptor& pushConstantRange) noexcept {
	VkPushConstantRange range = {};
	range.offset = pushConstantRange.offset;
	range.size = pushConstantRange.size;
	
	VkShaderStageFlags stages = static_cast<VkShaderStageFlags>(0);
	for (const auto& stage : pushConstantRange.bindingPipelineStage) {
		switch (stage) {
		case Shaders::ShaderType::Compute:
			stages |= VK_SHADER_STAGE_COMPUTE_BIT;
			break;
		case Shaders::ShaderType::Vertex:
			stages |= VK_SHADER_STAGE_VERTEX_BIT;
			break;
		case Shaders::ShaderType::Geometry:
			stages |= VK_SHADER_STAGE_GEOMETRY_BIT;
			break;
		case Shaders::ShaderType::Fragment:
			stages |= VK_SHADER_STAGE_FRAGMENT_BIT;
			break;
		}
	}

	range.stageFlags = stages;

	mPushConstants.emplace_back(std::move(range));
}

void ShaderLayoutBinding::insert(const BindingDescriptor& shaderBinding) noexcept {
	VkDescriptorType type;

	switch (shaderBinding.bindingType) {
	case BindingType::Sampler:
		type = VK_DESCRIPTOR_TYPE_SAMPLER;
		break;
	case BindingType::SampledImage:
		type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		break;
	case BindingType::CombinedImageSampler:
		type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		break;
	case BindingType::StorageImage:
		type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		break;
	case BindingType::UniformTexelStorage:
		type = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
		break;
	case BindingType::UniformTexelBuffer:
		type = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
		break;
	case BindingType::UniformBuffer:
		type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		break;
	case BindingType::StorageBuffer:
		type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		break;
	case BindingType::InputAttachment:
		type = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		break;
	default:
		VULKAN_DBG_ASSERT(false);
		break;
	}

	VkShaderStageFlags stages = static_cast<VkShaderStageFlags>(0);
	for (const auto& stage : shaderBinding.bindingPipelineStage) {
		switch (stage) {
		case Shaders::ShaderType::Compute:
			stages |= VK_SHADER_STAGE_COMPUTE_BIT;
			break;
		case Shaders::ShaderType::Vertex:
			stages |= VK_SHADER_STAGE_VERTEX_BIT;
			break;
		case Shaders::ShaderType::Geometry:
			stages |= VK_SHADER_STAGE_GEOMETRY_BIT;
			break;
		case Shaders::ShaderType::Fragment:
			stages |= VK_SHADER_STAGE_FRAGMENT_BIT;
			break;
		}
	}

	VkDescriptorSetLayoutBinding binding;
	binding.binding = shaderBinding.bindingPoint;
	binding.descriptorType = type;
	binding.descriptorCount = shaderBinding.count;
	binding.pImmutableSamplers = nullptr;
	binding.stageFlags = stages;

	mDescriptors.emplace_back(binding);
}

std::vector<VkDescriptorSetLayoutBinding> ShaderLayoutBinding::getNativeLayoutHandles() const noexcept {
	return mDescriptors;
}

std::vector<VkPushConstantRange> ShaderLayoutBinding::getNativePushContantsHandles() const noexcept {
	return mPushConstants;
}