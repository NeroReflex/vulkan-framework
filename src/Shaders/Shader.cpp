#include "Shaders/Shader.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;
using namespace NeroReflex::VulkanFramework::Shaders;

Shader::Shader(Device* device, ShaderType type, std::vector<VkDescriptorSetLayoutBinding>&& bindings, std::vector<VkPushConstantRange>&& pushConstants, VkShaderModule&& module) noexcept
	: DeviceOwned(device),
	mShaderType(type),
	mSpecializedBindings(std::move(bindings)),
	mSpecializedPushContants(std::move(pushConstants)),
	mShaderModule(std::move(module)) {}

Shader::~Shader() {
	vkDestroyShaderModule(getParentDevice()->getNativeDeviceHandle(), mShaderModule, nullptr);
}

const VkShaderModule& Shader::getNativeShaderModuleHandle() const noexcept {
	return mShaderModule;
}

const std::vector<VkDescriptorSetLayoutBinding>& Shader::getNativeShaderBindings() const noexcept {
	return mSpecializedBindings;
}

const std::vector<VkPushConstantRange>& Shader::getNativeShaderPushConstants() const noexcept {
	return mSpecializedPushContants;
}

const Shaders::ShaderType& Shader::getType() const noexcept {
	return mShaderType;
}

VkShaderStageFlagBits Shader::getStageFlagBits() const noexcept {
	if (mShaderType == Shaders::ShaderType::Vertex)
		return VK_SHADER_STAGE_VERTEX_BIT;
	else if (mShaderType == Shaders::ShaderType::Fragment)
		return VK_SHADER_STAGE_FRAGMENT_BIT;
	else if (mShaderType == Shaders::ShaderType::Geometry)
		return VK_SHADER_STAGE_GEOMETRY_BIT;

	return VK_SHADER_STAGE_COMPUTE_BIT;
}