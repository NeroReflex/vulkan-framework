#include "MemoryPool.h"
#include "Device.h"
#include "SpaceRequiringResource.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

MemoryPool::MemoryPool(Device* device, VkMemoryPropertyFlagBits props, uint32_t memoryTypeBits, VkDeviceSize pagesCount, VkDeviceMemory&& memory) noexcept
	: DeviceOwned(device),
	mProperties(std::move(props)),
	mMemoryTypeBits(memoryTypeBits),
	mTotalSize(pagesCount * Memory::atomicMemoryBlockSize),
	mDeviceMemory(std::move(memory)),
	mFixedPageTracker(::malloc(Memory::UnsafePoolManager::getManagementReservedSpace(pagesCount))),
	mPoolManager(pagesCount, NULL, mFixedPageTracker),
	mMappedMemory(nullptr) {}

MemoryPool::~MemoryPool() {
	::free(mFixedPageTracker);

	vkFreeMemory(getParentDevice()->getNativeDeviceHandle(), mDeviceMemory, nullptr);
}

const VkMemoryPropertyFlagBits& MemoryPool::getMemoryProperties() const noexcept {
	return mProperties;
}

const VkDeviceMemory& MemoryPool::getNativeDeviceMemoryHandle() const noexcept {
	return mDeviceMemory;
}

bool MemoryPool::isCompatible(const SpaceRequiringResource* res) const noexcept {
	const auto requirements = res->getMemoryRequirements();

	return (mMemoryTypeBits & requirements.memoryTypeBits) == mMemoryTypeBits;
}

bool MemoryPool::malloc(SpaceRequiringResource* res) noexcept {
	VULKAN_DBG_ASSERT( (this->getParentDevice() == res->getParentDevice()) );
	VULKAN_DBG_ASSERT( (res->getAllocationMemoryPool() == nullptr) );
	VULKAN_DBG_ASSERT( (isCompatible(res)) );

	const auto requirements = res->getMemoryRequirements();

	const auto allocResult = mPoolManager.malloc(requirements.size, requirements.alignment);

	if (allocResult.success) {
		res->bindMemory(this, static_cast<VkDeviceSize>(uintptr_t(allocResult.result)));
	}
	
	return allocResult.success;
}

void MemoryPool::free(SpaceRequiringResource* res) noexcept {
	const auto requirements = res->getMemoryRequirements();
	mPoolManager.free((void*)(uintptr_t(res->getAllocationOffset())), requirements.size, requirements.alignment);
}

VkDeviceSize MemoryPool::getAtomicMemoryBlockCount(const VkDeviceSize& size, const VkDeviceSize& alignment, const VkDeviceSize& prev) noexcept {
	return prev + 1 + ((alignment / Memory::atomicMemoryBlockSize) + (((alignment % Memory::atomicMemoryBlockSize) == 0) ? 0 : 1)) + ((size / Memory::atomicMemoryBlockSize) + (((size % Memory::atomicMemoryBlockSize) == 0) ? 0 : 1));
}

void* MemoryPool::mapMemory(VkDeviceSize offset, VkDeviceSize size) noexcept {
	VULKAN_DBG_ASSERT( (mProperties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) );
	VULKAN_DBG_ASSERT( ((offset + size) <= mTotalSize) );

	VULKAN_DBG_ASSERT( (mMappedMemory == nullptr) );

	void** ppPointer = &mMappedMemory;

	VK_CHECK_RESULT(vkMapMemory(getParentDevice()->getNativeDeviceHandle(), mDeviceMemory, offset, size, 0, ppPointer));

	return *ppPointer;
}

void MemoryPool::unmapMemory() noexcept {
	VULKAN_DBG_ASSERT((mMappedMemory != nullptr));

	vkUnmapMemory(getParentDevice()->getNativeDeviceHandle(), mDeviceMemory);

	mMappedMemory = nullptr;
}

VkMemoryPropertyFlagBits MemoryPool::getCompatibleMemoryPoolPropertyFlagBits(const std::vector<const SpaceRequiringResource*>& unallocatedResources) {
	// Allocate all resources that were not previously allocated
	uint32_t memoryTypeBits = 0xFFFFFFFF;
	VkDeviceSize minimumBlockCount = MemoryPool::getAtomicMemoryBlockCount(0, 0, 0);
	for (const auto& allocRequiringResource : unallocatedResources) {
		const auto requiredMemory = allocRequiringResource->getMemoryRequirements();
		minimumBlockCount += MemoryPool::getAtomicMemoryBlockCount(static_cast<VkDeviceSize>(requiredMemory.size), static_cast<VkDeviceSize>(requiredMemory.alignment), minimumBlockCount);
		memoryTypeBits &= requiredMemory.memoryTypeBits;
	}

	VULKAN_DBG_ASSERT((memoryTypeBits != 0));
	VULKAN_DBG_ASSERT((minimumBlockCount > 0));

	return static_cast<VkMemoryPropertyFlagBits>(memoryTypeBits);
}

VkDeviceSize MemoryPool::getCompatibleMemoryPoolSize(const std::vector<const SpaceRequiringResource*>& unallocatedResources) {
	// Allocate all resources that were not previously allocated
	uint32_t memoryTypeBits = 0xFFFFFFFF;
	VkDeviceSize minimumBlockCount = MemoryPool::getAtomicMemoryBlockCount(0, 0, 0);
	for (const auto& allocRequiringResource : unallocatedResources) {
		const auto requiredMemory = allocRequiringResource->getMemoryRequirements();
		minimumBlockCount += MemoryPool::getAtomicMemoryBlockCount(static_cast<VkDeviceSize>(requiredMemory.size), static_cast<VkDeviceSize>(requiredMemory.alignment), minimumBlockCount);
		memoryTypeBits &= requiredMemory.memoryTypeBits;
	}

	VULKAN_DBG_ASSERT((memoryTypeBits != 0));
	VULKAN_DBG_ASSERT((minimumBlockCount > 0));

	return minimumBlockCount;
}