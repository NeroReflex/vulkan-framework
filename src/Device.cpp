#include "Device.h"

#include "ComputePipeline.h"
#include "Framebuffer.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

VkPresentModeKHR Device::SwapchainSelector::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) const noexcept {
	for (const auto& availablePresentMode : availablePresentModes) {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
            return availablePresentMode;
        }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkSurfaceFormatKHR Device::SwapchainSelector::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) const noexcept {
    for (const auto& availableFormat : availableFormats) {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return availableFormat;
        }
    }

    return availableFormats[0];
}

VkSwapchainCreateInfoKHR Device::SwapchainSelector::operator()(const SwapChainSupportDetails& swapChainSupport) const noexcept {
	VkSwapchainCreateInfoKHR createInfo;

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
    VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);

	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
    	imageCount = swapChainSupport.capabilities.maxImageCount;
	}

	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.presentMode = presentMode;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;

	return createInfo;
}

Device::Device(
	const Instance* instance,
	VkPhysicalDevice&& physicalDevice,
	VkDevice&& device,
	std::vector<std::tuple<QueueFamily::ConcreteQueueFamilyDescriptor, uint32_t>> requiredQueueFamilyCollection
) noexcept
	: InstanceOwned(instance),
	mPhysicalDevice(physicalDevice),
	mDevice(device) {
	// Query avaliable extensions
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(mPhysicalDevice, nullptr, &extensionCount, nullptr);
	
	mAvailableExtensions.resize(extensionCount);
	vkEnumerateDeviceExtensionProperties(mPhysicalDevice, nullptr, &extensionCount, mAvailableExtensions.data());

	for (const auto& requiredQueueFamily : requiredQueueFamilyCollection) {
		mQueueFamilies.push_back(new QueueFamily(this, std::get<0>(requiredQueueFamily), std::get<1>(requiredQueueFamily)));
	}

}

Device::~Device() {
	// Remove each object: it is important that this is done BEFORE removing the current device
	mOwnedObjects.clear();

	vkDestroyDevice(mDevice, nullptr);
}

const QueueFamily* Device::getQueueFamily(uint32_t index) const noexcept {
	VULKAN_DBG_ASSERT((index < (mQueueFamilies.size())));

	return mQueueFamilies[index];
}

bool Device::isExtensionAvailable(const std::string& extName) const noexcept {
	for (const auto& extension : mAvailableExtensions) {
		if (strcmp(extName.c_str(), extension.extensionName) == 0) {
			return true;
		}
	}

	return false;
}

const VkDevice& VulkanFramework::Device::getNativeDeviceHandle() const noexcept {
	return mDevice;
}

const VkPhysicalDevice& VulkanFramework::Device::getNativePhysicalDeviceInstance() const noexcept {
	return mPhysicalDevice;
}

Shaders::ComputeShader* Device::loadComputeShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept {
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.codeSize = size;
	createInfo.pCode = reinterpret_cast<const uint32_t*>(source);
	createInfo.flags = 0; // At the time of writing reserved for future use
	std::vector<VkDescriptorSetLayoutBinding> mSpecializedBindings(std::move(bindings.getNativeLayoutHandles()));
	for (auto& binding : mSpecializedBindings) {
		binding.stageFlags |= VK_SHADER_STAGE_COMPUTE_BIT;
	}

	std::vector<VkPushConstantRange> mSpecializedPushConstants(std::move(bindings.getNativePushContantsHandles()));
	for (auto& pushConstant : mSpecializedPushConstants) {
		pushConstant.stageFlags |= VK_SHADER_STAGE_COMPUTE_BIT;
	}

	VkShaderModule shaderModule;
	VK_CHECK_RESULT(vkCreateShaderModule(getNativeDeviceHandle(), &createInfo, nullptr, &shaderModule));
	
	return new Shaders::ComputeShader(this, std::move(mSpecializedBindings), std::move(mSpecializedPushConstants), std::move(shaderModule));
}

Shaders::VertexShader* Device::loadVertexShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept {
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.codeSize = size;
	createInfo.pCode = reinterpret_cast<const uint32_t*>(source);
	createInfo.flags = 0; // At the time of writing reserved for future use
	std::vector<VkDescriptorSetLayoutBinding> mSpecializedBindings(std::move(bindings.getNativeLayoutHandles()));
	for (auto& binding : mSpecializedBindings) {
		binding.stageFlags |= VK_SHADER_STAGE_VERTEX_BIT;
	}

	std::vector<VkPushConstantRange> mSpecializedPushConstants(std::move(bindings.getNativePushContantsHandles()));
	for (auto& pushConstant : mSpecializedPushConstants) {
		pushConstant.stageFlags |= VK_SHADER_STAGE_VERTEX_BIT;
	}

	VkShaderModule shaderModule;
	VK_CHECK_RESULT(vkCreateShaderModule(getNativeDeviceHandle(), &createInfo, nullptr, &shaderModule));

	return new Shaders::VertexShader(this, std::move(mSpecializedBindings), std::move(mSpecializedPushConstants), std::move(shaderModule));
}

Shaders::GeometryShader* Device::loadGeometryShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept {
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.codeSize = size;
	createInfo.pCode = reinterpret_cast<const uint32_t*>(source);
	createInfo.flags = 0; // At the time of writing reserved for future use
	std::vector<VkDescriptorSetLayoutBinding> mSpecializedBindings(std::move(bindings.getNativeLayoutHandles()));
	for (auto& binding : mSpecializedBindings) {
		binding.stageFlags |= VK_SHADER_STAGE_GEOMETRY_BIT;
	}

	std::vector<VkPushConstantRange> mSpecializedPushConstants(std::move(bindings.getNativePushContantsHandles()));
	for (auto& pushConstant : mSpecializedPushConstants) {
		pushConstant.stageFlags |= VK_SHADER_STAGE_GEOMETRY_BIT;
	}

	VkShaderModule shaderModule;
	VK_CHECK_RESULT(vkCreateShaderModule(getNativeDeviceHandle(), &createInfo, nullptr, &shaderModule));

	return new Shaders::GeometryShader(this, std::move(mSpecializedBindings), std::move(mSpecializedPushConstants), std::move(shaderModule));
}

Shaders::FragmentShader* Device::loadFragmentShader(const ShaderLayoutBinding& bindings, const char* source, uint32_t size) noexcept {
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.codeSize = size;
	createInfo.pCode = reinterpret_cast<const uint32_t*>(source);
	createInfo.flags = 0; // At the time of writing reserved for future use
	std::vector<VkDescriptorSetLayoutBinding> mSpecializedBindings(std::move(bindings.getNativeLayoutHandles()));
	for (auto& binding : mSpecializedBindings) {
		binding.stageFlags |= VK_SHADER_STAGE_FRAGMENT_BIT;
	}

	std::vector<VkPushConstantRange> mSpecializedPushConstants(std::move(bindings.getNativePushContantsHandles()));
	for (auto& pushConstant : mSpecializedPushConstants) {
		pushConstant.stageFlags |= VK_SHADER_STAGE_FRAGMENT_BIT;
	}

	VkShaderModule shaderModule;
	VK_CHECK_RESULT(vkCreateShaderModule(getNativeDeviceHandle(), &createInfo, nullptr, &shaderModule));

	return new Shaders::FragmentShader(this, std::move(mSpecializedBindings), std::move(mSpecializedPushConstants), std::move(shaderModule));
}

ComputePipeline* Device::createComputePipeline(const Shaders::ComputeShader* shader) noexcept {
	VULKAN_DBG_ASSERT( (shader->getType() == Shaders::ShaderType::Compute) );

	std::vector<VkDescriptorSetLayoutBinding> renderDescriptorSetLayoutBinding;
	std::vector<VkPushConstantRange> renderDescriptorSetPushConstantRanges;

	VkPipelineShaderStageCreateInfo shaderStageInfo = {};
	shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	shaderStageInfo.pNext = nullptr;
	shaderStageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
	shaderStageInfo.module = shader->getNativeShaderModuleHandle();
	shaderStageInfo.pName = "main";
	shaderStageInfo.pSpecializationInfo = nullptr;
	
	const auto& shaderBindings = shader->getNativeShaderBindings();
	renderDescriptorSetLayoutBinding.insert(renderDescriptorSetLayoutBinding.cend(), shaderBindings.cbegin(), shaderBindings.cend());
	const auto& shaderPushContants = shader->getNativeShaderPushConstants();
	renderDescriptorSetPushConstantRanges.insert(renderDescriptorSetPushConstantRanges.cend(), shaderPushContants.cbegin(), shaderPushContants.cend());

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.pNext = NULL;
	descriptorSetLayoutCreateInfo.flags = 0;
	descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(renderDescriptorSetLayoutBinding.size());
	descriptorSetLayoutCreateInfo.pBindings = renderDescriptorSetLayoutBinding.data();

	VkDescriptorSetLayout descriptorSetLayout;
	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(mDevice, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout));

	VkPipelineLayout pipelineLayout;
	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutCreateInfo.pushConstantRangeCount = static_cast<uint32_t>(renderDescriptorSetPushConstantRanges.size());
	pipelineLayoutCreateInfo.pPushConstantRanges = renderDescriptorSetPushConstantRanges.data(); // Optional

	VK_CHECK_RESULT(vkCreatePipelineLayout(mDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	Pipeline* result = nullptr;

	VkPipeline pipeline;
	VkComputePipelineCreateInfo computePipelineCreateInfo;
	computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	computePipelineCreateInfo.pNext = nullptr;
	computePipelineCreateInfo.flags = 0;
	computePipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;

#if defined(VULKAN_ENABLE_VALIDATION_LAYERS)
	computePipelineCreateInfo.flags |= VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT;
#endif
	computePipelineCreateInfo.stage = shaderStageInfo;
	computePipelineCreateInfo.layout = pipelineLayout;

	VK_CHECK_RESULT(vkCreateComputePipelines(mDevice, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, nullptr, &pipeline));

	return new ComputePipeline(this, std::move(pipelineLayout), std::move(descriptorSetLayout), std::move(pipeline));
}

GraphicPipeline* Device::createGraphicPipeline(
	const RenderPass* renderPass,
	uint32_t subpassNumber,
	const std::vector<const Shaders::Shader*>& shaders,
	Utils::Rasterizer rasterizer,
	Utils::DepthStencilConfiguration depthStencilConfig,
	const Utils::SurfaceDimensions& surfaceDimensions,
	const std::vector<VertexInputBinding>& bindings
) noexcept
{
	VULKAN_DBG_ASSERT( (shaders.size() > 0) );
	bool isComputePipeline = (shaders.size() == 1) && (shaders[0]->getType() == Shaders::ShaderType::Compute);

	std::vector<VkPipelineShaderStageCreateInfo> shadersStageInfo(shaders.size());
	std::vector<VkDescriptorSetLayoutBinding> renderDescriptorSetLayoutBinding;
	std::vector<VkPushConstantRange> renderPushConstantRanges;
	
	uint32_t i = 0;
	for (const auto& shader : shaders) {
		shadersStageInfo[i].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		shadersStageInfo[i].pNext = nullptr;
		shadersStageInfo[i].stage = shader->getStageFlagBits();
		shadersStageInfo[i].module = shader->getNativeShaderModuleHandle();
		shadersStageInfo[i].pName = "main";
		shadersStageInfo[i].pSpecializationInfo = nullptr;

		std::vector<VkDescriptorSetLayoutBinding> shaderBindings(shader->getNativeShaderBindings());
		for (auto& shaderBinding : shaderBindings) {
			shaderBinding.stageFlags |= shader->getStageFlagBits();
		}

		renderDescriptorSetLayoutBinding.insert(renderDescriptorSetLayoutBinding.cend(), shaderBindings.cbegin(), shaderBindings.cend());

		std::vector<VkPushConstantRange> shaderPushContants(shader->getNativeShaderPushConstants());
		for (auto& shaderPushConstant : shaderPushContants) {
			shaderPushConstant.stageFlags |= shader->getStageFlagBits();
		}

		renderPushConstantRanges.insert(renderPushConstantRanges.cend(), shaderPushContants.cbegin(), shaderPushContants.cend());

		++i;
	}

	VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
	descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetLayoutCreateInfo.pNext = NULL;
	descriptorSetLayoutCreateInfo.flags = 0;
	descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(renderDescriptorSetLayoutBinding.size()); // only a single binding in this descriptor set layout. 
	descriptorSetLayoutCreateInfo.pBindings = renderDescriptorSetLayoutBinding.data();

	VkDescriptorSetLayout descriptorSetLayout;
	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(mDevice, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout));
	
	VkPipelineLayout pipelineLayout;
	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutCreateInfo.pushConstantRangeCount = static_cast<uint32_t>(renderPushConstantRanges.size()); // Optional
	pipelineLayoutCreateInfo.pPushConstantRanges = renderPushConstantRanges.data(); // Optional

	VK_CHECK_RESULT(vkCreatePipelineLayout(mDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	// Draw only triangles
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.pNext = nullptr;
	inputAssembly.flags = 0; // flags is reserved for future use
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	// This creates the vertex input state
	std::vector<VkVertexInputBindingDescription> vertexInputBindings;
	std::vector<VkVertexInputAttributeDescription> vertexInputAttributes;
	uint32_t bindingNumber = 0;
	for (const auto& binding : bindings) {

		auto nativeHandlers = binding.generateNativeHandlers(bindingNumber);

		vertexInputBindings.emplace_back(std::get<0>(nativeHandlers));
		for (const auto& attributeOnBinding : std::get<1>(nativeHandlers))
			vertexInputAttributes.emplace_back(attributeOnBinding);

		bindingNumber++;
	}
	VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInput = {};
	vertexInputStateCreateInput.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputStateCreateInput.pNext = nullptr;
	vertexInputStateCreateInput.flags = 0; // reserved for future use at the moment of writing
	vertexInputStateCreateInput.vertexAttributeDescriptionCount = vertexInputAttributes.size();
	vertexInputStateCreateInput.pVertexAttributeDescriptions = vertexInputAttributes.data();
	vertexInputStateCreateInput.vertexBindingDescriptionCount = vertexInputBindings.size();
	vertexInputStateCreateInput.pVertexBindingDescriptions = vertexInputBindings.data();
	
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = renderPass->getSampleCount();
	multisampling.minSampleShading = 1.0f; // Optional
	multisampling.pSampleMask = nullptr; // Optional
	multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
	multisampling.alphaToOneEnable = VK_FALSE; // Optional

	// Create the viewport
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = static_cast<float>(surfaceDimensions.getWidth());
	viewport.height = static_cast<float>(surfaceDimensions.getHeight());
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	// Create the scissor
	VkExtent2D extent = {};
	extent.width = surfaceDimensions.getWidth();
	extent.height = surfaceDimensions.getHeight();
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = extent;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments(renderPass->getSubpassByIndex(subpassNumber).getColorAttachmentIndeces().size());
	for (uint32_t si = 0; si < colorBlendAttachments.size(); ++si) {
		colorBlendAttachments[si].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachments[si].blendEnable = VK_FALSE;
		colorBlendAttachments[si].srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachments[si].dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachments[si].colorBlendOp = VK_BLEND_OP_ADD; // Optional
		colorBlendAttachments[si].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachments[si].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachments[si].alphaBlendOp = VK_BLEND_OP_ADD; // Optional
	}

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
	colorBlending.attachmentCount = static_cast<uint32_t>(colorBlendAttachments.size());
	colorBlending.pAttachments = colorBlendAttachments.data();
	colorBlending.blendConstants[0] = 0.0f; // Optional
	colorBlending.blendConstants[1] = 0.0f; // Optional
	colorBlending.blendConstants[2] = 0.0f; // Optional
	colorBlending.blendConstants[3] = 0.0f; // Optional

	std::vector<VkDynamicState> dynamicStates = { VK_DYNAMIC_STATE_VIEWPORT };
	VkPipelineDynamicStateCreateInfo dynamicState = {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
	dynamicState.pDynamicStates = dynamicStates.data();

	// depth/stencil structure
	VkPipelineDepthStencilStateCreateInfo depthStencilCreateInfo = {};
	depthStencilCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilCreateInfo.pNext = nullptr;
	depthStencilCreateInfo.flags = 0; // reserved for future use
	depthStencilCreateInfo.depthTestEnable = depthStencilConfig.getEnableDepthTesting() ? VK_TRUE : VK_FALSE;
	depthStencilCreateInfo.depthWriteEnable = depthStencilConfig.getDepthWriteEnable() ? VK_TRUE : VK_FALSE;
	depthStencilCreateInfo.depthCompareOp = depthStencilConfig.getDepthCompareOperation();
	depthStencilCreateInfo.depthBoundsTestEnable = depthStencilConfig.getDepthBoundsTestEnable() ? VK_TRUE : VK_FALSE;
	depthStencilCreateInfo.stencilTestEnable = depthStencilConfig.getStencilTestEnable() ? VK_TRUE : VK_FALSE;
	depthStencilCreateInfo.front = depthStencilConfig.getFront();
	depthStencilCreateInfo.back = depthStencilConfig.getBack();
	depthStencilCreateInfo.minDepthBounds = depthStencilConfig.getMinDepthBounds();
	depthStencilCreateInfo.maxDepthBounds = depthStencilConfig.getMaxDepthBounds();

	// Fill the main structure
	VkGraphicsPipelineCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.basePipelineHandle = VK_NULL_HANDLE; // create a pipeline from another one
	createInfo.basePipelineIndex = -1; // create a pipeline from another one
	createInfo.layout = pipelineLayout;
	createInfo.pInputAssemblyState = &inputAssembly;
	createInfo.pVertexInputState = &vertexInputStateCreateInput;
	createInfo.stageCount = static_cast<uint32_t>(shadersStageInfo.size());
	createInfo.pStages = shadersStageInfo.data();
	createInfo.pViewportState = &viewportState;
	createInfo.pRasterizationState = &(rasterizer.getNativeRasterizationCreateInfoHandler());
	createInfo.pColorBlendState = &colorBlending;
	createInfo.pDepthStencilState = &depthStencilCreateInfo;
	createInfo.pDynamicState = nullptr; // TODO: maybe use ( &dynamicState ) .... Skipped because IDK how to deal with dynamic states
	createInfo.pMultisampleState = &multisampling;
	createInfo.renderPass = renderPass->getNativeRenderPassHandle();
	createInfo.subpass = subpassNumber;

	VULKAN_DBG_ASSERT((subpassNumber < renderPass->getSubpassesCount()));

	VkPipeline pipeline;
	VK_CHECK_RESULT(vkCreateGraphicsPipelines(getNativeDeviceHandle(), VK_NULL_HANDLE, 1, &createInfo, nullptr, &pipeline));

	return new GraphicPipeline(this, std::move(pipelineLayout), std::move(descriptorSetLayout), std::move(pipeline), std::move(rasterizer), std::move(depthStencilConfig),surfaceDimensions, renderPass);
}

RenderPass* Device::createRenderPass(
	const std::vector<VkAttachmentDescription>& attachments,
	const std::vector<RenderPass::RenderSubPass>& subpasses,
	const std::vector<VkSubpassDependency>& subpassesDependencies
) noexcept {
	/*
	The index of the attachment in this array is directly referenced from the fragment shader with the layout(location = 0) out vec4 outColor directive!
	*/
	std::vector<VkSubpassDescription> subpassDefinitions(subpasses.size());
	std::vector<std::vector<VkAttachmentReference>> inputAttachmentReferencesBySubpass(subpasses.size());
	std::vector<std::vector<VkAttachmentReference>> colorAttachmentReferencesBySubpass(subpasses.size());
	std::vector<VkAttachmentReference> depthStencilAttachmentReferencesBySubpass(subpasses.size());

	uint32_t i = 0;
	for (const auto& subpass : subpasses) {
		VkAttachmentReference ref = {};
		for (const auto& colorAttachment : subpass.getColorAttachmentIndeces()) {
			VULKAN_DBG_ASSERT((colorAttachment < attachments.size()));

			ref.attachment = colorAttachment;
			ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			colorAttachmentReferencesBySubpass[i].emplace_back(ref);
		}

		for (const auto& inputAttachment : subpass.getInputColorAttachmentIndeces()) {
			VULKAN_DBG_ASSERT((inputAttachment < attachments.size()));

			ref.attachment = inputAttachment;
			ref.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

			inputAttachmentReferencesBySubpass[i].emplace_back(ref);
		}

		ref.attachment = (subpass.getDepthStencilAttachmentIndex().has_value()) ? subpass.getDepthStencilAttachmentIndex().value() : 0;
		ref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		depthStencilAttachmentReferencesBySubpass[i] = ref;

		VULKAN_DBG_ASSERT(((!subpass.getDepthStencilAttachmentIndex().has_value()) || (subpass.getDepthStencilAttachmentIndex().value() < attachments.size())));
		
		subpassDefinitions[i].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpassDefinitions[i].colorAttachmentCount = static_cast<uint32_t>(colorAttachmentReferencesBySubpass[i].size());
		subpassDefinitions[i].pColorAttachments = (colorAttachmentReferencesBySubpass[i].empty()) ? nullptr : colorAttachmentReferencesBySubpass[i].data();
		subpassDefinitions[i].inputAttachmentCount = static_cast<uint32_t>(inputAttachmentReferencesBySubpass[i].size());
		subpassDefinitions[i].pInputAttachments = (inputAttachmentReferencesBySubpass[i].empty()) ? nullptr : inputAttachmentReferencesBySubpass[i].data();

		if (subpass.getDepthStencilAttachmentIndex().has_value()) {
			subpassDefinitions[i].pDepthStencilAttachment = &depthStencilAttachmentReferencesBySubpass[i];
		}
		
		++i;
	}

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.pNext = nullptr;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = static_cast<uint32_t>(subpassDefinitions.size());
	renderPassInfo.pSubpasses = subpassDefinitions.data();
	renderPassInfo.dependencyCount = static_cast<uint32_t>(subpassesDependencies.size());
	renderPassInfo.pDependencies = subpassesDependencies.data();
	VkRenderPass renderPass = VK_NULL_HANDLE;
	
	VULKAN_DBG_ONLY(
		{
			if (!attachments.empty()) {
				const auto sample = attachments[0].samples;
				for (const auto& attachment : attachments) {
					VULKAN_DBG_ASSERT((sample == attachment.samples));
				}
			}
		}
	);

	VK_CHECK_RESULT(vkCreateRenderPass(mDevice, &renderPassInfo, nullptr, &renderPass));

	return new RenderPass(
		this,
		std::move(renderPass),
		subpasses,
		subpassesDependencies,
		(attachments.empty()) ? VK_SAMPLE_COUNT_1_BIT : attachments[0].samples
	);
}

Swapchain* Device::createSwapchain(VkSurfaceKHR surface, std::vector<const QueueFamily*> queueFamilyCollection, uint32_t width, uint32_t height, const SwapchainSelector& selector) noexcept {
	VULKAN_DBG_ASSERT( (surface != VK_NULL_HANDLE) );

	// Query swapchain support
	{	
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(mPhysicalDevice, surface, &mSupportedSwapchain.capabilities);

		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(mPhysicalDevice, surface, &formatCount, nullptr);
		if (formatCount != 0) {
			mSupportedSwapchain.formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(mPhysicalDevice, surface, &formatCount, mSupportedSwapchain.formats.data());
		}

		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(mPhysicalDevice, surface, &presentModeCount, nullptr);

		if (presentModeCount != 0) {
			mSupportedSwapchain.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(mPhysicalDevice, surface, &presentModeCount, mSupportedSwapchain.presentModes.data());
		}

		VULKAN_DBG_ASSERT((isExtensionAvailable(VK_KHR_SWAPCHAIN_EXTENSION_NAME)));

		VULKAN_DBG_ASSERT(((!mSupportedSwapchain.formats.empty()) && (!mSupportedSwapchain.presentModes.empty())));
	}
	
	VULKAN_DBG_ASSERT((queueFamilyCollection.size() >= 1));

	VkExtent2D actualExtent = { width, height };

	std::vector<uint32_t> sharingQueueFamilyCollection;
	for (const auto& sharingQueue : queueFamilyCollection) {
		sharingQueueFamilyCollection.push_back(sharingQueue->getNativeQueueFamilyIndexHandle());
	}

	VkSwapchainCreateInfoKHR createInfo = selector(mSupportedSwapchain);
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	createInfo.oldSwapchain = VK_NULL_HANDLE;
	createInfo.surface = surface;
	createInfo.imageSharingMode = (queueFamilyCollection.size() > 1) ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
    createInfo.queueFamilyIndexCount = static_cast<uint32_t>(sharingQueueFamilyCollection.size());
    createInfo.pQueueFamilyIndices = sharingQueueFamilyCollection.data();
	actualExtent.width = std::max(mSupportedSwapchain.capabilities.minImageExtent.width, std::min(mSupportedSwapchain.capabilities.maxImageExtent.width, actualExtent.width));
    actualExtent.height = std::max(mSupportedSwapchain.capabilities.minImageExtent.height, std::min(mSupportedSwapchain.capabilities.maxImageExtent.height, actualExtent.height));
	createInfo.imageExtent = actualExtent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = static_cast<VkImageUsageFlags>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR /*VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR*/;
	createInfo.clipped = VK_TRUE;
	createInfo.flags = 0;
	
	VkBool32 isSurfaceSupported = VK_FALSE;
	VK_CHECK_RESULT(vkGetPhysicalDeviceSurfaceSupportKHR(mPhysicalDevice, queueFamilyCollection[0]->getNativeQueueFamilyIndexHandle(), surface, &isSurfaceSupported));
	VULKAN_DBG_ASSERT((isSurfaceSupported == VK_TRUE));

	VkSwapchainKHR swapchain;
	VK_CHECK_RESULT(vkCreateSwapchainKHR(mDevice, &createInfo, nullptr, &swapchain));

	return new Swapchain(this, std::move(swapchain), createInfo.imageFormat, std::move(Utils::SurfaceDimensions(actualExtent.width, actualExtent.height)));
}

Sampler* Device::createSampler(Sampler::Filtering magFilter, Sampler::Filtering minFilter, Sampler::MipmapMode mipmapMode, float maxAnisotropy) noexcept {
	VkSamplerCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0; // Hardcoded: is it something I should not? If needed just add another parameter :P
	
	switch (magFilter) {
		case Sampler::Filtering::Nearest:
			createInfo.magFilter = VK_FILTER_NEAREST;
			break;
		case Sampler::Filtering::Linear:
			createInfo.magFilter = VK_FILTER_LINEAR;
			break;
		case Sampler::Filtering::Cubic:
			createInfo.magFilter = VK_FILTER_CUBIC_IMG;
			break;
		default:
			VULKAN_DBG_ASSERT( false );
	}

	switch (minFilter) {
	case Sampler::Filtering::Nearest:
		createInfo.minFilter = VK_FILTER_NEAREST;
		break;
	case Sampler::Filtering::Linear:
		createInfo.minFilter = VK_FILTER_LINEAR;
		break;
	case Sampler::Filtering::Cubic:
		createInfo.minFilter = VK_FILTER_CUBIC_IMG;
		break;
	default:
		VULKAN_DBG_ASSERT(false);
	}
	
	switch (mipmapMode) {
	case Sampler::MipmapMode::ModeNearest:
		createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
		break;
	case Sampler::MipmapMode::ModeLinear:
		createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		break;
	default:
		VULKAN_DBG_ASSERT(false);
	}

	createInfo.unnormalizedCoordinates = VK_FALSE;
	createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK; // VK_BORDER_COLOR_INT_OPAQUE_WHITE
	createInfo.anisotropyEnable = (maxAnisotropy >= 1.0f) ? VK_TRUE : VK_FALSE;
	createInfo.maxAnisotropy = maxAnisotropy;
	createInfo.compareEnable = VK_FALSE;

	VkSampler sampler;
	VK_CHECK_RESULT(vkCreateSampler(mDevice, &createInfo, nullptr, &sampler));

	return new Sampler(this, std::move(sampler), magFilter, minFilter, mipmapMode, (maxAnisotropy >= 1.0) ? maxAnisotropy : 0.0f);
}

Image* Device::createImage(
	std::vector<const QueueFamily*> queueFamilyCollection,
	VkImageUsageFlags usage,
	Image::ImageType type,
	uint32_t width,
	uint32_t height,
	uint32_t depth,
	VkFormat format,
	uint32_t mipLevels,
	VkSampleCountFlagBits samples
) noexcept {

	VULKAN_DBG_ASSERT( (!queueFamilyCollection.empty()) );

	VkImageType imgType = VK_IMAGE_TYPE_3D;
	switch (type) {
	case Image::ImageType::Image1D:
		imgType = VK_IMAGE_TYPE_1D;
		height = 1;
		depth = 1;
		break;

	case Image::ImageType::Image2D:
		imgType = VK_IMAGE_TYPE_2D;
		depth = 1;
		break;

	case Image::ImageType::Image3D:
		imgType = VK_IMAGE_TYPE_3D;
		break;

	default:
		VULKAN_DBG_ASSERT(false);
	}

	std::vector<uint32_t> sharingQueueFamilyCollection;
	for (const auto& sharingQueue : queueFamilyCollection) {
		sharingQueueFamilyCollection.push_back(sharingQueue->getNativeQueueFamilyIndexHandle());
	}

	VkImageCreateInfo imageCreateInfo; // Using stack will lead to stack overflow
	imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageCreateInfo.pNext = nullptr;
	imageCreateInfo.flags = VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT;
	imageCreateInfo.sharingMode = (sharingQueueFamilyCollection.size() > 1) ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
	imageCreateInfo.queueFamilyIndexCount = static_cast<uint32_t>(sharingQueueFamilyCollection.size());
	imageCreateInfo.pQueueFamilyIndices = sharingQueueFamilyCollection.data();
	imageCreateInfo.format = format;
	imageCreateInfo.samples = samples;
	imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageCreateInfo.usage = usage;
	imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageCreateInfo.mipLevels = mipLevels;
	imageCreateInfo.arrayLayers = 1;
	imageCreateInfo.imageType = imgType;
	imageCreateInfo.extent.width = width;
	imageCreateInfo.extent.height = height;
	imageCreateInfo.extent.depth = depth;

	// This is to create the ModelMatrix-Collection
	VkImage image;
	VK_CHECK_RESULT(vkCreateImage(mDevice, &imageCreateInfo, nullptr, &image));

	return new Image(this, type, format, imageCreateInfo.extent, samples, mipLevels, std::move(image));
}

MemoryPool* Device::createMemoryPool(VkMemoryPropertyFlagBits props, VkDeviceSize blockCount, uint32_t memoryTypeBits) noexcept {
	const auto sizeInBytes = blockCount * Memory::atomicMemoryBlockSize;

	VkMemoryAllocateInfo allocateInfo = {};
	allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocateInfo.pNext = nullptr;
	allocateInfo.allocationSize = sizeInBytes;
	allocateInfo.memoryTypeIndex = findHeap(memoryTypeBits, props);

	// Allocate a big chunk of memory on the device
	VkDeviceMemory memoryPool;
	VK_CHECK_RESULT(vkAllocateMemory(mDevice, &allocateInfo, NULL, &memoryPool));

	return new MemoryPool(this, props, memoryTypeBits, blockCount, std::move(memoryPool));
}

MemoryPool* Device::createMemoryPool(VkMemoryPropertyFlagBits props, const std::vector<const SpaceRequiringResource*>& unallocatedResources, VkDeviceSize freePagesCount) noexcept {
	const auto blockCount = MemoryPool::getCompatibleMemoryPoolSize(unallocatedResources) + freePagesCount;
	const auto memoryTypeBits = static_cast<uint32_t>(MemoryPool::getCompatibleMemoryPoolPropertyFlagBits(unallocatedResources));

	return this->createMemoryPool(props, blockCount, memoryTypeBits);
}

CommandPool* Device::createCommandPool(const QueueFamily* queueFamily) noexcept {
	VkCommandPoolCreateInfo commandPoolCreateInfo = {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    // the queue family of this command pool. All command buffers allocated from this command pool,
	// must be submitted to queues of this family ONLY. 
	commandPoolCreateInfo.queueFamilyIndex = queueFamily->getNativeQueueFamilyIndexHandle();
	
	VkCommandPool cmdPool;
	VK_CHECK_RESULT(vkCreateCommandPool(mDevice, &commandPoolCreateInfo, nullptr, &cmdPool));
	
	return new CommandPool(this, std::move(cmdPool));
}

uint32_t Device::findHeap(uint32_t memoryTypeBits, VkMemoryPropertyFlags properties) const noexcept {
	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(mPhysicalDevice, &memoryProperties);

	uint32_t candidate = 0;
	bool found = false;

	/*
	How does this search work?
	See the documentation of VkPhysicalDeviceMemoryProperties for a detailed description.
	*/
	for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i) {
		if ((memoryTypeBits & (1 << i)) &&
			((memoryProperties.memoryTypes[i].propertyFlags & properties) == properties)) {
				candidate = i;
				found = true;
			}
	}

	VULKAN_DBG_ASSERT( (found) );

	return candidate;
}

DescriptorPool* Device::createDescriptorPool(const std::vector<std::tuple<ShaderLayoutBinding::BindingType, uint32_t>>& descriptorPoolSize, uint32_t maxSets) noexcept {
	VULKAN_DBG_ASSERT((maxSets > 0));
	
	std::vector<VkDescriptorPoolSize> descriptorPoolSizeNativeCollection(descriptorPoolSize.size());
	for (uint32_t k = 0; k < descriptorPoolSize.size(); ++k) {
		VkDescriptorType nativeType;
		switch (std::get<0>(descriptorPoolSize[k])) {
		case ShaderLayoutBinding::BindingType::Sampler:
			nativeType = VK_DESCRIPTOR_TYPE_SAMPLER;
			break;
		case ShaderLayoutBinding::BindingType::CombinedImageSampler:
			nativeType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			break;
		case ShaderLayoutBinding::BindingType::SampledImage:
			nativeType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
			break;
		case ShaderLayoutBinding::BindingType::StorageImage:
			nativeType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
			break;
		case ShaderLayoutBinding::BindingType::UniformTexelBuffer:
			nativeType = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
			break;
		case ShaderLayoutBinding::BindingType::UniformTexelStorage:
			nativeType = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
			break;
		case ShaderLayoutBinding::BindingType::StorageBuffer:
			nativeType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
			break;
		case ShaderLayoutBinding::BindingType::UniformBuffer:
			nativeType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			break;
		case ShaderLayoutBinding::BindingType::InputAttachment:
			nativeType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
			break;
		default:
			VULKAN_DBG_ASSERT(false);
		}

		descriptorPoolSizeNativeCollection[k].type = nativeType;

		VULKAN_DBG_ASSERT((std::get<1>(descriptorPoolSize[k]) > 0));
		descriptorPoolSizeNativeCollection[k].descriptorCount = std::get<1>(descriptorPoolSize[k]);
	}

	VkDescriptorPoolCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.maxSets = maxSets;
	createInfo.poolSizeCount = static_cast<uint32_t>(descriptorPoolSizeNativeCollection.size());
	createInfo.pPoolSizes = descriptorPoolSizeNativeCollection.data();
	createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

	VkDescriptorPool descriptorPool;
	VK_CHECK_RESULT(vkCreateDescriptorPool(mDevice, &createInfo, nullptr, &descriptorPool));

	return new DescriptorPool(this, std::move(descriptorPool));
}

Fence* Device::createFence(bool signaled) noexcept {
	VkFence fence;

	VkFenceCreateInfo fenceCreateInfo = {};
	fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceCreateInfo.flags = (signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0);
	VK_CHECK_RESULT(vkCreateFence(getNativeDeviceHandle(), &fenceCreateInfo, NULL, &fence));

	return new Fence(this, std::move(fence));
}

Semaphore* Device::createSemaphore() noexcept {
	VkSemaphoreCreateInfo createSemaphoreInfo = {};
	createSemaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	createSemaphoreInfo.pNext = nullptr;
	createSemaphoreInfo.flags = 0; // At the time of writing reserved for future use

	VkSemaphore semaphore;
	vkCreateSemaphore(getNativeDeviceHandle(), &createSemaphoreInfo, nullptr, &semaphore);

	return new Semaphore(this, std::move(semaphore));
}

Buffer* Device::createBuffer(std::vector<const QueueFamily*> queueFamilyCollection, VkBufferUsageFlags usage, VkDeviceSize size) noexcept {
	std::vector<uint32_t> sharingQueueFamilyCollection;
	for (const auto& sharingQueue : queueFamilyCollection) {
		sharingQueueFamilyCollection.push_back(sharingQueue->getNativeQueueFamilyIndexHandle());
	}

	VkBufferCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.usage = usage;
	createInfo.sharingMode = (sharingQueueFamilyCollection.size() > 1) ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = static_cast<uint32_t>(sharingQueueFamilyCollection.size());
	createInfo.pQueueFamilyIndices = sharingQueueFamilyCollection.data();
	createInfo.size = size;

	VkBuffer buffer;
	vkCreateBuffer(getNativeDeviceHandle(), &createInfo, nullptr, &buffer);

	return new Buffer(this, std::move(usage), std::move(size),std::move(buffer));
}

void Device::waitForFences(std::vector<const Fence*> fences, uint64_t timeout) const noexcept {
	std::vector<VkFence> nativeFences;

	for (const auto& fence : fences) nativeFences.push_back(fence->getNativeFanceHandle());

	VK_CHECK_RESULT(vkWaitForFences(mDevice, nativeFences.size(), nativeFences.data(), VK_TRUE, timeout));
}

Framebuffer* Device::createFramebuffer(
	const RenderPass* const renderPass,
	const Utils::SurfaceDimensions& surfaceDimensions,
	const std::vector<ImageView*>& imageViews
) noexcept {
	
	//VULKAN_DBG_ASSERT( (!imageViews.empty()) ); // TODO: check that the number of attachment must meet the number of imageViews

	std::vector<VkImageView> nativeImageViews(imageViews.size());
	for (uint32_t i = 0; i < imageViews.size(); ++i) {
		nativeImageViews[i] = imageViews[i]->getNativeImageViewHandle();
	}

	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	//framebufferInfo.flags = VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT; // TODO: Enable the appropriate extension (should be core now) and move in a createImagelessFramebuffer()
	framebufferInfo.renderPass = renderPass->getNativeRenderPassHandle();
	framebufferInfo.attachmentCount = static_cast<uint32_t>(nativeImageViews.size());
	framebufferInfo.pAttachments = nativeImageViews.data();
	framebufferInfo.width = surfaceDimensions.getWidth();
	framebufferInfo.height = surfaceDimensions.getHeight();
	framebufferInfo.layers = 1;

	VkFramebuffer framebuffer;
	VK_CHECK_RESULT(vkCreateFramebuffer(mDevice, &framebufferInfo, nullptr, &framebuffer));

	return new Framebuffer(this, std::move(framebuffer), imageViews);
}

void Device::waitIdle() const noexcept {
	vkDeviceWaitIdle(getNativeDeviceHandle());
}

void Device::destroy(DeviceOwned* const obj) noexcept {
	// search the given object in the owned object list and remove it
	// calling erase on the unique_ptr holding the owned object will
	// result in a call to delete and thus C++ destructor for the specified object
	const auto selfIt = mOwnedObjects.find(uintptr_t(obj));
	if (selfIt != mOwnedObjects.cend()) {
		mOwnedObjects.erase(selfIt);
	}
}
