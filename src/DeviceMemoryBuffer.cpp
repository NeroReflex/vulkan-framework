#include "DeviceMemoryBuffer.h"
#include "Device.h"

using namespace NeroReflex;
using namespace NeroReflex::VulkanFramework;

DeviceMemoryBuffer::DeviceMemoryBuffer(Device* device) noexcept : DeviceOwned(device) {}

DeviceMemoryBuffer::~DeviceMemoryBuffer() {}