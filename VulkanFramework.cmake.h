#pragma once

// STL streams
#include <iostream>
#include <fstream>
#include <sstream>

// STL data types
#include <functional>
#include <string>
#include <optional>

// STL containers
#include <initializer_list>
#include <unordered_map>
#include <map>
#include <vector>
#include <array>
#include <list>
#include <set>

// STL memory
#include <memory>

// STL algorithms
#include <algorithm>
#include <utility>
#include <limits>

// GLM math library
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include <vulkan/vulkan.h>

#if !defined(NDEBUG)
#include <assert.h>
#define VULKAN_DBG_ASSERT(x) assert(x)
#define VULKAN_DBG_ONLY(x) x
#else
#define VULKAN_DBG_ASSERT(x) /* x */
#define VULKAN_DBG_ONLY(x) /* x */
#endif

#if !defined(NDEBUG)
#cmakedefine VULKAN_ENABLE_VALIDATION_LAYERS 1
#define VK_CHECK_RESULT(f) 																				\
		{																										\
			VkResult res = (f);																					\
			if (res != VK_SUCCESS)																				\
			{																									\
				printf("Fatal : VkResult is %d in %s at line %d\n", res,  __FILE__, __LINE__); \
				VULKAN_DBG_ASSERT(res == VK_SUCCESS);																		\
			}																									\
		}
#else
#define VK_CHECK_RESULT(f) f
#endif

// Remove macros that generate compile errors
#undef max
#undef min

constexpr size_t roundupToMultipleOfVec4(size_t in) {
	return (in / sizeof(glm::vec4)) + sizeof(glm::vec4);
}
